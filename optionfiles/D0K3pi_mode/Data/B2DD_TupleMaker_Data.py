"""
 Script to create a DecayTreeTuple for B2DD analysis:
"""
__author__ = "Frank Meier <frank.meier@tu-dortmund.de>"

##############################################################################################
##### SETTINGS
##############################################################################################
import GaudiKernel.SystemOfUnits as Units
from Gaudi.Configuration import *
from DecayTreeTuple.Configuration import *
from Configurables import (BDecayTool,
                           BTagging,
                           BTaggingTool,
                           CheckPV,
                           CombineParticles,
                           DecayTreeTuple,
                           EventTuple,
                           FilterDesktop,
                           FitDecayTrees,
                           GaudiSequencer,
                           L0TriggerTisTos,
                           LoKi__Hybrid__TupleTool,
                           OfflineVertexFitter,
                           PrintDecayTree,
                           PrintDecayTreeTool,
                           TaggingUtilsChecker,
                           TESCheck,
                           TriggerTisTos,
                           TupleToolAngles,
                           TupleToolDecay,
                           TupleToolDecayTreeFitterExtended,
                           TupleToolRecoStats,
                           TupleToolTagging,
                           TupleToolTISTOS,
                           TupleToolStripping,
                           TupleToolTrackPosition,
                           TupleToolTrigger,
                           TupleToolEventInfo,
                           TupleToolVtxIsoln,
                           TupleToolGeometry,
                           TupleToolTrackInfo,
                           TupleToolPid,
                           TupleToolANNPID,
                           TupleToolMassHypo)
import re
from PhysSelPython.Wrappers import Selection, SelectionSequence
from StandardParticles import StdLoosePions, StdAllNoPIDsPions
from Configurables import TrackScaleState
from Configurables import TrackSmearState as SMEAR
from Configurables import CondDB

from Configurables import DaVinci 
DaVinci().RootInTES = '/Event/BhadronCompleteEvent' 
from Configurables import TrackScaleState as SCALER 
scaler = SCALER( 'Scaler' , RootInTES =  "/Event/BhadronCompleteEvent") 
DaVinci().UserAlgorithms += [ scaler ]

prefix = '/Event/BhadronCompleteEvent/'
##############################################################################################
##### IMPORTS
##############################################################################################
from PhysSelPython.Wrappers import Selection, SelectionSequence
from StandardParticles import StdLoosePions, StdAllNoPIDsPions
##############################################################################################
##### PRESELECTION
##############################################################################################
from Configurables import LoKi__HDRFilter as StripFilter

from PhysSelPython.Wrappers import DataOnDemand
inputDataOnDemandB2DD = DataOnDemand(Location = prefix + 'Phys/B02DDBeauty2CharmLine/Particles')

_tighterDs = FilterDesktop("TighterDs")

D1MassWindow = "( CHILDCUT( ADMASS('D+')<50*MeV, 1)) "
D2MassWindow = "( CHILDCUT( ADMASS('D+')<50*MeV, 2)) "
DMassWindowL = "(" + D1MassWindow + " & " + D2MassWindow + ")"


D1MassWindowA = "( CHILDCUT( ADMASS('D_s+')<50*MeV, 1)) "
D2MassWindowA = "( CHILDCUT( ADMASS('D+')<50*MeV, 2)) "
DMassWindowA = "(" + D1MassWindowA + " & " + D2MassWindowA + ")"


D1MassWindowB = "( CHILDCUT( ADMASS('D_s+')<50*MeV, 2)) "
D2MassWindowB = "( CHILDCUT( ADMASS('D+')<50*MeV, 1)) "
DMassWindowB = "(" + D1MassWindowB + " & " + D2MassWindowB + ")"

DMassWindow = "(" + DMassWindowL + "|" + DMassWindowB + "|" + DMassWindowA + ")"

_tighterDs.Code = DMassWindow
SelTighterDs = Selection("SelTighterDs", Algorithm = _tighterDs, RequiredSelections = [inputDataOnDemandB2DD])
SelSeqTighterDs = SelectionSequence("SelSeqTighterDs", TopSelection = SelTighterDs)
seqDs = SelSeqTighterDs.sequence()

##############################################################################################
##### Upgrade of _TRACK_GhostProb for downstream tracks
##############################################################################################
from STTools import STOfflineConf
STOfflineConf.DefaultConf().configureTools()
from Configurables import RefitParticleTracks
refitter = RefitParticleTracks()
refitter.DoFit = True
# shouldn't make a difference. either confirm that, or leave it as it is
refitter.DoProbability = True
refitter.UpdateProbability = True
refitter.ReplaceTracks = True
# shouldn't make a difference. either confirm that, or leave it as it is

## I never fully convinced myself whether the following three lines are
## necessary or not. Either way they're not wrong:
from Configurables import TrackInitFit, TrackMasterFitter
from TrackFitter.ConfiguredFitters import ConfiguredMasterFitter

refitter.addTool(TrackInitFit,"TrackInitFit")
refitter.TrackInitFit.addTool(TrackMasterFitter,"Fit")
ConfiguredMasterFitter( refitter.TrackInitFit.Fit )

B2DDtuple = DecayTreeTuple("B02DD")

B2DDtuple.Inputs = [SelSeqTighterDs.outputLocation()]
# dominant final state for DD
#B2DDtuple.Decay = "[B0 -> ^(D+ -> ^K- ^pi+ ^pi+) ^(D- -> ^K+ ^pi- ^pi-)]CC"
# dominant DsD final state and subdominant DD final state
B2DDtuple.Decay  = "[(B0 -> ^(D+ -> ^(K-) ^(pi+) ^(pi+)) ^(D- -> ^(K+) ^(K-) ^(pi-))) || "
B2DDtuple.Decay  += "(B0 -> ^(D+ -> ^(K-) ^(K+) ^(pi+)) ^(D- -> ^(K+) ^(pi-) ^(pi-)))]CC"
# third DD final state
#B2DDtuple.Decay  = "[B0 -> ^(D+ -> ^(K-) ^(K+) ^(pi+)) ^(D- -> ^(K+) ^(K-) ^(pi-))]CC"
# any final state
#B2DDtuple.Decay = "[B0 -> ^(D+ -> ^(K- || pi-) ^(pi+ || K+) ^(pi+ || K+)) ^(D- -> ^(K+ || pi+) ^(pi- || K-) ^(pi- || K-))]CC"
B2DDtuple.ReFitPVs = False
##############################################################################################
##### STANDARD TOOLS AND STRIPPING LINE
##############################################################################################
tuple_tools = ["TupleToolKinematic",
               "TupleToolPropertime",
               "TupleToolRecoStats" ,
               "TupleToolPrimaries"]

tuple_tools_mc = ["TupleToolMCTruth",
                  "TupleToolMCBackgroundInfo"]

B2DDtuple.ToolList = tuple_tools

tt_pid = B2DDtuple.addTupleTool("TupleToolPid")
tt_pid.Verbose = True

tt_geometry = B2DDtuple.addTupleTool("TupleToolGeometry")
tt_geometry.Verbose = False
tt_geometry.RefitPVs = False
tt_geometry.FillMultiPV = False

tt_trackinfo = B2DDtuple.addTupleTool("TupleToolTrackInfo") # matchChi2 etc.
tt_trackinfo.Verbose = True

tt_eventinfo = B2DDtuple.addTupleTool("TupleToolEventInfo")

branches = {}
descriptor_B = "([B0 -> (D+ -> (K- || pi-) (pi+ || K+) (pi+ || K+)) (D- -> (K+ || pi+) (pi- || K-) (pi- || K-))]CC)"
branches["B0"] = "^(" + descriptor_B + ")"

add_branches = { "Dplus"  : ( "D+", { #"substitute"                  : [ {"K-" : "K-"}, {"K-" : "pi-"}, {"pi+" : "K+"} ],
                                      #"LOKI_DOCA"                   : "ACUTDOCA",
                                      "LOKI_VertexSeparation_CHI2"  : "BPVVDCHI2" }),
                 "Dminus" : ( "D-", { #"substitute"                  : [ {"K+" : "K+"}, {"K+" : "pi+"}, {"pi-" : "K-"} ],
                                      #"LOKI_DOCA"                   : "ACUTDOCA",
                                      "LOKI_VertexSeparation_CHI2"  : "BPVVDCHI2" })}

for name_branch, value in add_branches.iteritems():
  branches[name_branch] = descriptor_B.replace("(" + value[0], "^(" + value[0])
  B2DDtuple.addTupleTool(TupleToolDecay, name=name_branch)
  branch_infos = value[1]
  if "substitute" in branch_infos:
    substitutions = branch_infos.pop("substitute")
    i = 0
    for substitution in substitutions:
      tt_masshypo = B2DDtuple.__getattr__(name_branch).addTupleTool("TupleToolMassHypo/TTMassHypo_" + str(i))
      tt_masshypo.PIDReplacements = substitution
      i += 1
  if len(branch_infos) > 0:
    tt_loki_br = B2DDtuple.__getattr__(name_branch).addTupleTool("LoKi::Hybrid::TupleTool/tt_loki_"+name_branch)
    tt_loki_br.Variables = branch_infos
  else:
    print "For branch " + name_branch + " no other LOKI variables specified."

branches["Dplus_Kminus_or_piminus"] = "[B0 -> (D+ -> ^(K- || pi-) (pi+ || K+) (pi+ || K+)) (D- -> (K+ || pi+) (pi- || K-) (pi- || K-))]CC"
branches["Dplus_piplus_or_Kplus_One"] = "[B0 -> (D+ -> (K- || pi-) ^(pi+ || K+) (pi+ || K+)) (D- -> (K+ || pi+) (pi- || K-) (pi- || K-))]CC"
branches["Dplus_piplus_or_Kplus_Two"] = "[B0 -> (D+ -> (K- || pi-) (pi+ || K+) ^(pi+ || K+)) (D- -> (K+ || pi+) (pi- || K-) (pi- || K-))]CC"
branches["Dminus_Kplus_or_piplus"] = "[B0 -> (D+ -> (K- || pi-) (pi+ || K+) (pi+ || K+)) (D- -> ^(K+ || pi+) (pi- || K-) (pi- || K-))]CC"
branches["Dminus_piminus_or_Kminus_One"] = "[B0 -> (D+ -> (K- || pi-) (pi+ || K+) (pi+ || K+)) (D- -> (K+ || pi+) ^(pi- || K-) (pi- || K-))]CC"
branches["Dminus_piminus_or_Kminus_Two"] = "[B0 -> (D+ -> (K- || pi-) (pi+ || K+) (pi+ || K+)) (D- -> (K+ || pi+) (pi- || K-) ^(pi- || K-))]CC"

B2DDtuple.addBranches(branches)

B2DDtuple.addTupleTool(TupleToolDecay, name="B0")

##############################################################################################
##### TAGGING (not available before Stripping 21)
##############################################################################################
from Configurables import (BTaggingTool,
                           OSMuonTagger,
                           OSElectronTagger,
                           OSKaonTagger,
                           OSVtxChTagger)
OptimizationVersion = 'Summer2017OptimisationDev'

from FlavourTagging.Tunings import applyTuning as applyFTTuning
tt_tagging1         = B2DDtuple.addTupleTool("TupleToolTagging")
tt_tagging1.Verbose = True
tt_tagging1.AddMVAFeatureInfo = False
tt_tagging1.AddTagPartsInfo   = False
tt_tagging1.OutputLevel       = INFO
btagtool1                     = tt_tagging1.addTool(BTaggingTool, name = "MyBTaggingTool1")
applyFTTuning(btagtool1, tuning_version= OptimizationVersion )
tt_tagging1.TaggingToolName   = btagtool1.getFullName()
tt_tagging1.Verbose           = True
##############################################################################################
##### TRIGGER
##############################################################################################

hlt1_lines = ['L0PhysicsDecision',
              'L0MuonDecision',
              'L0DiMuonDecision',
              'L0MuonHighDecision',
              'L0HadronDecision',
              'L0ElectronDecision',
              'L0PhotonDecision',
              'Hlt1TrackAllL0Decision',
              'Hlt1TrackAllL0TightDecision',
              'Hlt1L0AnyDecision',
              'Hlt1GlobalDecision']

hlt2_lines = ['Hlt2GlobalDecision'
              """
              'Hlt2Topo2BodySimpleDecision',
              'Hlt2Topo3BodySimpleDecision',
              'Hlt2Topo4BodySimpleDecision',
              """
              ,"Hlt1TrackAllL0Decision"
              ,"Hlt1TrackMVADecision"
              ,"Hlt1TwoTrackMVADecision"
              ,"Hlt2Topo2BodyDecision"
              ,"Hlt2Topo3BodyDecision"
              ,"Hlt2Topo4BodyDecision"
              ,'Hlt2Topo2BodyBBDTDecision'
              ,'Hlt2Topo3BodyBBDTDecision'
              ,'Hlt2Topo4BodyBBDTDecision'
              # 'Hlt2TopoMu2BodyBBDTDecision',
              # 'Hlt2TopoMu3BodyBBDTDecision',
              # 'Hlt2TopoMu4BodyBBDTDecision',
              # 'Hlt2TopoE2BodyBBDTDecision',
              # 'Hlt2TopoE3BodyBBDTDecision',
              # 'Hlt2TopoE4BodyBBDTDecision',
              ,'Hlt2IncPhiDecision'
              ,'Hlt2IncPhiSidebandsDecision']

trigger_lines = hlt1_lines + hlt2_lines
tt_trigger = B2DDtuple.addTupleTool("TupleToolTrigger")
tt_trigger.Verbose = True
tt_trigger.TriggerList = trigger_lines

tt_tistos = B2DDtuple.__getattr__("B0").addTupleTool("TupleToolTISTOS")
tt_tistos.Verbose = True
tt_tistos.TriggerList = trigger_lines

##############################################################################################
##### LOKI VARIABLES
##############################################################################################

loki_variables = {"LOKI_ENERGY"     : "E",
                  "LOKI_ETA"        : "ETA",
                  "LOKI_PHI"        : "PHI"}

tt_loki_general = B2DDtuple.addTupleTool("LoKi::Hybrid::TupleTool/tt_loki_general")
tt_loki_general.Variables = loki_variables

loki_variables_B = {"LOKI_FDCHI2"                 : "BPVVDCHI2",
                    "LOKI_FDS"                    : "BPVDLS",
                    "LOKI_DIRA"                   : "BPVDIRA",
                    ###### DecayTreeFitVariables
                    "LOKI_DTF_CTAU"               : "DTF_CTAU( 0, True )",
                    "LOKI_DTF_CTAU_NOPV"          : "DTF_CTAU( 0, False )",
                    "LOKI_DTF_CTAUS"              : "DTF_CTAUSIGNIFICANCE( 0, True )",
                    "LOKI_DTF_CHI2NDOF"           : "DTF_CHI2NDOF( True )",
                    "LOKI_DTF_CTAUERR"            : "DTF_CTAUERR( 0, True )",
                    "LOKI_DTF_CTAUERR_NOPV"       : "DTF_CTAUERR( 0, False )",
                    "LOKI_MASS_DplusConstr"       : "DTF_FUN ( M , True , 'D+' )" ,
                    "LOKI_MASS_Dplus_NoPVConstr"  : "DTF_FUN ( M , False , 'D+' )" ,
                    "LOKI_MASS_DminusConstr"      : "DTF_FUN ( M , True , 'D-' )" ,
                    "LOKI_MASS_Dminus_NoPVConstr" : "DTF_FUN ( M , False , 'D-' )" ,
                    "LOKI_MASSERR_DplusConstr"    : "sqrt(DTF_FUN ( M2ERR2 , True , 'D+' ))" ,
                    "LOKI_MASSERR_DminusConstr"   : "sqrt(DTF_FUN ( M2ERR2 , True , 'D-' ))" ,
                    "LOKI_DTF_VCHI2NDOF"          : "DTF_FUN ( VFASPF(VCHI2/VDOF) , True )",
                    "LOKI_DTF_CTAU_D1"            : "DTF_CTAU(1, True)",
                    "LOKI_DTF_CTAU_D2"            : "DTF_CTAU(2, True)",
                    "LOKI_DTF_CTAUERR_D1"         : "DTF_CTAUERR(1, True)",
                    "LOKI_DTF_CTAUERR_D2"         : "DTF_CTAUERR(2, True)",
                    "LOKI_MASS_DDConstr"          : "DTF_FUN ( M , True , strings ( ['D+','D-'] ) )" ,
                    "LOKI_MASSERR_DDConstr"       : "sqrt(DTF_FUN ( M2ERR2 , True , strings(['D+','D-'])))"}

tt_loki_B = B2DDtuple.__getattr__("B0").addTupleTool("LoKi::Hybrid::TupleTool/tt_loki_B")
tt_loki_B.Variables = loki_variables_B

##############################################################################################
##### CONSTRAINTS
##############################################################################################

#No constraints at all
Fit = B2DDtuple.__getattr__("B0").addTupleTool("TupleToolDecayTreeFitterExtended/Fit")
Fit.Verbose = True
#Fit.OutputLevel = VERBOSE
Fit.UpdateDaughters = True
Fit.constrainToOriginVertex = False

#Constraint only on PV 
FitPVConst = B2DDtuple.__getattr__("B0").addTupleTool("TupleToolDecayTreeFitterExtended/FitPVConst")
FitPVConst.Verbose = True
#FitPVConst.OutputLevel = VERBOSE
FitPVConst.UpdateDaughters = True
FitPVConst.constrainToOriginVertex = True

#Constraint on daughters but not on PV
FitDaughtersConst = B2DDtuple.__getattr__("B0").addTupleTool("TupleToolDecayTreeFitterExtended/FitDDConst")
FitDaughtersConst.Verbose = True
#FitDaughtersConst.OutputLevel = VERBOSE
FitDaughtersConst.UpdateDaughters = True
FitDaughtersConst.daughtersToConstrain = ["D+","D-"]
FitDaughtersConst.constrainToOriginVertex = False

#Constraint on daughters and constraint to OriginPV
FitDaughtersPVConst = B2DDtuple.__getattr__("B0").addTupleTool("TupleToolDecayTreeFitterExtended/FitDDPVConst")
FitDaughtersPVConst.Verbose = True
#FitDaughtersPVConst.OutputLevel = VERBOSE
FitDaughtersPVConst.UpdateDaughters = True
FitDaughtersPVConst.daughtersToConstrain = ["D+","D-"]
FitDaughtersPVConst.constrainToOriginVertex = True

FitDaughters_strange1Const = B2DDtuple.__getattr__("B0").addTupleTool("TupleToolDecayTreeFitterExtended/FitDsDConst")
#FitDaughters_strange1Const.Verbose = True
#FitDaughters_strange1PVConst.OutputLevel = VERBOSE
#FitDaughters_strange1Const.UpdateDaughters = True
FitDaughters_strange1Const.UpdateDaughters = False
FitDaughters_strange1Const.daughtersToConstrain = ["D_s+","D-"]
FitDaughters_strange1Const.constrainToOriginVertex = False
FitDaughters_strange1Const.Substitutions = { "B0 -> ^(D+ -> K- K+ pi+) (D- -> K+ pi- pi-)" : "D_s+"}


##Constraint on daughters and constraint to OriginPV
FitDaughters_strange2Const = B2DDtuple.__getattr__("B0").addTupleTool("TupleToolDecayTreeFitterExtended/FitDDsConst")
#FitDaughters_strange2Const.Verbose = True
##FitDaughters_strange2Const.OutputLevel = VERBOSE
#FitDaughters_strange2Const.UpdateDaughters = True
FitDaughters_strange2Const.UpdateDaughters = False
FitDaughters_strange2Const.daughtersToConstrain = ["D+","D_s-"]
FitDaughters_strange2Const.constrainToOriginVertex = False
FitDaughters_strange2Const.Substitutions = { "B0 -> (D+ -> K- pi+ pi+) ^(D- -> K+ K- pi-)" : "D_s-"}
#
##Constraint on daughters and constraint to OriginPV
FitDaughters_strange1PVConst = B2DDtuple.__getattr__("B0").addTupleTool("TupleToolDecayTreeFitterExtended/FitDsDPVConst")
#FitDaughters_strange1PVConst.Verbose = True
#FitDaughters_strange1PVConst.OutputLevel = VERBOSE
#FitDaughters_strange1PVConst.UpdateDaughters = True
FitDaughters_strange1PVConst.UpdateDaughters = False
FitDaughters_strange1PVConst.daughtersToConstrain = ["D_s+","D-"]
FitDaughters_strange1PVConst.constrainToOriginVertex = True
FitDaughters_strange1PVConst.Substitutions = { "B0 -> ^(D+ -> K- K+ pi+) (D- -> K+ pi- pi-)" : "D_s+"}
#
#
##Constraint on daughters and constraint to OriginPV
FitDaughters_strange2PVConst = B2DDtuple.__getattr__("B0").addTupleTool("TupleToolDecayTreeFitterExtended/FitDDsPVConst")
#FitDaughters_strange2PVConst.Verbose = True
##FitDaughters_strange2PVConst.OutputLevel = VERBOSE
#FitDaughters_strange2PVConst.UpdateDaughters = True
FitDaughters_strange2PVConst.UpdateDaughters = False
FitDaughters_strange2PVConst.daughtersToConstrain = ["D+","D_s-"]
FitDaughters_strange2PVConst.constrainToOriginVertex = True
FitDaughters_strange2PVConst.Substitutions = { "B0 -> (D+ -> K- pi+ pi+) ^(D- -> K+ K- pi-)" : "D_s-"}

# Particle substitutions in DTF
#FitSubstitution = B2DDtuple.__getattr__("B0").addTupleTool("TupleToolDecayTreeFitterExtended/FitSubstKpiminus")
#FitSubstitution.Verbose = True
#FitSubstitution.UpdateDaughters = True
#FitSubstitution.daughtersToConstrain = ["D-"]
#FitSubstitution.constrainToOriginVertex = True
#
#new_decay = "[B0 -> (D+ -> K- pi+ pi+) (D- -> ^K+ pi- pi-)]CC"
#dtf_substitution = { new_decay : "pi+"}
#FitSubstitution = B2DDtuple.__getattr__("B0").addTupleTool("TupleToolDecayTreeFitterExtended/FitSubstKpiplus")
#FitSubstitution.Verbose = True
#FitSubstitution.UpdateDaughters = True
#FitSubstitution.daughtersToConstrain = ["D+"]
#FitSubstitution.constrainToOriginVertex = True
#FitSubstitution.Substitutions = dtf_substitution

# if DaVinci().Simulation == False:
#   for input in B2DDtuple.Inputs:
#     if input not in refitter.Inputs:
#       print "Adding " + input + " to refitter"
#       refitter.Inputs.append(input)

###########################################################
seqB2DD                  = GaudiSequencer('seqB2DD')
TupleSeq                 = GaudiSequencer('TupleSeq')

seqB2DD.Members         += [seqDs]
seqB2DD.Members         += [B2DDtuple]

##############################################################################################
##### IMPORTS
##############################################################################################
tuple = DecayTreeTuple("B02DstD")
tuple.Inputs = [prefix + 'Phys/B02DstDBeauty2CharmLine/Particles'] #stripping Line
tuple.Decay = "[ ([B0]cc -> ^( D*(2010)- -> ^pi- ^( [D~0]cc -> ^K+ ^(K- || pi-) )) ^(D+ -> ^K- ^(K+ || pi+) ^pi+)) ]CC"
				# [([B0]cc ->  ( D*(2010)- ->  pi- ( [D~0]cc ->  K+  (K- || pi-)))  ^(D+ ->  K-  (K+ || pi+)  pi+))]CC
tuple.ReFitPVs = False

##############################################################################################
##### STANDARD TOOLS AND STRIPPING LINE
##############################################################################################
tuple_tools = ["TupleToolKinematic",
               "TupleToolPropertime",
               "TupleToolTrackIsolation",
               "TupleToolPrimaries"]
tuple.ToolList = tuple_tools
tt_pid = tuple.addTupleTool("TupleToolPid")
tt_pid.Verbose = True
tt_trackpos = tuple.addTupleTool("TupleToolTrackPosition")
tt_trackpos.Z = 7500.
tt_recostats = tuple.addTupleTool("TupleToolRecoStats")
tt_recostats.Verbose = True
tt_geometry = tuple.addTupleTool("TupleToolGeometry")
tt_geometry.Verbose = False
tt_geometry.RefitPVs = False
tt_geometry.FillMultiPV = False
tt_trackinfo = tuple.addTupleTool("TupleToolTrackInfo") # matchChi2 etc.
tt_trackinfo.Verbose = True
tt_eventinfo = tuple.addTupleTool("TupleToolEventInfo")
tt_eventinfo.Verbose = True
tt_stripping = tuple.addTupleTool("TupleToolStripping")
tt_stripping.Verbose = True
tt_stripping.StrippingList = ['StrippingB02DstDBeauty2CharmLineDecision',
                              'StrippingB02DDBeauty2CharmLineDecision']
tuple.addBranches({
    'B0'		: '[([B0]cc ->  ( D*(2010)- ->  pi-  ( [D~0]cc ->  K+  (K- || pi-)))  (D+ ->  K-  (K+ || pi+)  pi+))]CC',
    'Dstar'		: '[([B0]cc -> ^( D*(2010)- ->  pi-  ( [D~0]cc ->  K+  (K- || pi-)))  (D+ ->  K-  (K+ || pi+)  pi+))]CC',
    'slowpi'	: '[([B0]cc ->  ( D*(2010)- -> ^pi-  ( [D~0]cc ->  K+  (K- || pi-)))  (D+ ->  K-  (K+ || pi+)  pi+))]CC',
    'D0'		: '[([B0]cc ->  ( D*(2010)- ->  pi- ^( [D~0]cc ->  K+  (K- || pi-)))  (D+ ->  K-  (K+ || pi+)  pi+))]CC',
    'pi_D0'		: '[([B0]cc ->  ( D*(2010)- ->  pi-  ( [D~0]cc ->  K+ ^(K- || pi-)))  (D+ ->  K-  (K+ || pi+)  pi+))]CC',
    'K_D0'		: '[([B0]cc ->  ( D*(2010)- ->  pi-  ( [D~0]cc -> ^K+  (K- || pi-)))  (D+ ->  K-  (K+ || pi+)  pi+))]CC',
    'D'			: '[([B0]cc ->  ( D*(2010)- ->  pi-  ( [D~0]cc ->  K+  (K- || pi-))) ^(D+ ->  K-  (K+ || pi+)  pi+))]CC',
    'K_D'		: '[([B0]cc ->  ( D*(2010)- ->  pi-  ( [D~0]cc ->  K+  (K- || pi-)))  (D+ -> ^K-  (K+ || pi+)  pi+))]CC',
    'pi_D'		: '[([B0]cc ->  ( D*(2010)- ->  pi-  ( [D~0]cc ->  K+  (K- || pi-)))  (D+ ->  K-  (K+ || pi+) ^pi+))]CC',
    'pi_or_K_D'	: '[([B0]cc ->  ( D*(2010)- ->  pi-  ( [D~0]cc ->  K+  (K- || pi-)))  (D+ ->  K- ^(K+ || pi+)  pi+))]CC',
})  
tuple.addTool(TupleToolDecay, name="B0")
##############################################################################################
##### TAGGING (not available before Stripping 21)
##############################################################################################
tt_tagging2 = tuple.addTupleTool("TupleToolTagging")
tt_tagging2.Verbose = True
tt_tagging2.AddMVAFeatureInfo = False
tt_tagging2.AddTagPartsInfo = False
tt_tagging2.OutputLevel = INFO
btagtool2 = tt_tagging2.addTool(BTaggingTool, name = "MyBTaggingTool2")
#applyFTTuning(btagtool, tuning_version="TupleDevelopment")
applyFTTuning(btagtool2, tuning_version= OptimizationVersion)
tt_tagging2.TaggingToolName = btagtool2.getFullName()
tt_tagging2.Verbose = True
##############################################################################################
##### TRIGGER
##############################################################################################
hlt1_lines = ['L0PhysicsDecision',
              'L0MuonDecision',
              'L0DiMuonDecision',
              'L0MuonHighDecision',
              'L0HadronDecision',
              'L0ElectronDecision',
              'L0PhotonDecision',
              'Hlt1TrackAllL0Decision',
              'Hlt1TrackAllL0TightDecision',
              'Hlt1L0AnyDecision',
              'Hlt1GlobalDecision',
              "Hlt1TrackAllL0Decision",
              "Hlt1TrackMVADecision",
              "Hlt1TwoTrackMVADecision"
              ]

hlt2_lines = ['Hlt2GlobalDecision'
              ,"Hlt2Topo2BodyDecision"
              ,"Hlt2Topo3BodyDecision"
              ,"Hlt2Topo4BodyDecision"
              ,'Hlt2Topo2BodyBBDTDecision'
              ,'Hlt2Topo3BodyBBDTDecision'
              ,'Hlt2Topo4BodyBBDTDecision'
              ,'Hlt2IncPhiDecision'
              ,'Hlt2IncPhiSidebandsDecision']

trigger_lines = hlt1_lines + hlt2_lines
tt_trigger = tuple.addTupleTool("TupleToolTrigger")
tt_trigger.Verbose = True
tt_trigger.TriggerList = trigger_lines

tt_tistos = tuple.__getattr__("B0").addTupleTool("TupleToolTISTOS")
tt_tistos.Verbose = True
tt_tistos.TriggerList = trigger_lines
##############################################################################################
##### LOKI VARIABLES
##############################################################################################
loki_variables = {"ENERGY"     : "E",
                  "ETA"        : "ETA",
                  "PHI"        : "PHI"}

tt_loki_general = tuple.addTupleTool("LoKi::Hybrid::TupleTool/tt_loki_general")
tt_loki_general.Variables = loki_variables

loki_variables_B = {"LOKI_FDCHI2"                    : "BPVVDCHI2",
                    "LOKI_FDS"                       : "BPVDLS",
                    "LOKI_DIRA"                      : "BPVDIRA",
                    "LOKI_DTF_TAU"                   : "DTF_CTAU( 0, True )/0.299792458",
                    "LOKI_DTF_CTAU"                  : "DTF_CTAU( 0, True )",
                    "LOKI_DTF_CTAU_NOPV"             : "DTF_CTAU( 0, False )",
                    "LOKI_DTF_CTAUS"                 : "DTF_CTAUSIGNIFICANCE( 0, True )",
                    "LOKI_DTF_CHI2NDOF"              : "DTF_CHI2NDOF( True )",
                    "LOKI_DTF_CTAUERR"               : "DTF_CTAUERR( 0, True )",
                    "LOKI_DTF_CTAUERR_NOPV"          : "DTF_CTAUERR( 0, False )",
                    "LOKI_MASS_DstarConstr"          : "DTF_FUN ( M , True , 'D*(2010)-' )" ,
                    "LOKI_MASS_Dstar_NoPVConstr"     : "DTF_FUN ( M , False , 'D*(2010)-' )" ,
                    "LOKI_MASS_DplusConstr"          : "DTF_FUN ( M , True , 'D+' )" ,
                    "LOKI_MASS_Dplus_NoPVConstr"     : "DTF_FUN ( M , False , 'D+' )" ,
                    "LOKI_MASSERR_DstarConstr"       : "sqrt(DTF_FUN ( M2ERR2 , True , 'D*(2010)-' ))" ,
                    "LOKI_MASSERR_DplusConstr"       : "sqrt(DTF_FUN ( M2ERR2 , True , 'D+' ))" ,
                    "LOKI_DTF_VCHI2NDOF"             : "DTF_FUN ( VFASPF(VCHI2/VDOF) , True )",
                    "LOKI_DTF_CTAU_D1"               : "DTF_CTAU(1, True)",
                    "LOKI_DTF_CTAU_D2"               : "DTF_CTAU(2, True)",
                    "LOKI_DTF_CTAUERR_D1"            : "DTF_CTAUERR(1, True)",
                    "LOKI_DTF_CTAUERR_D2"            : "DTF_CTAUERR(2, True)",
                    "LOKI_DTF_MASS_3D"               : "DTF_FUN ( M , True , strings( ['D*(2010)+', 'D0' , 'D+'] ) )" ,
                    "LOKI_DTF_VCHI2NDOF_3D"          : "DTF_FUN ( VFASPF(VCHI2/VDOF) , True , strings( ['D*(2010)+', 'D0' , 'D+'] ) )" ,
                    "LOKI_DTF_MASS_DstD0"            : "DTF_FUN ( M , True , strings( ['D*(2010)+', 'D0'] ))" ,
                    "LOKI_DTF_MASS_DstDp"            : "DTF_FUN ( M , True , strings( ['D*(2010)+', 'D+'] ))" ,
                    "LOKI_DTF_MASS_D0Dp"             : "DTF_FUN ( M , True , strings( ['D0', 'D+'] ))" ,
                    "LOKI_DTF_MASS_D0"               : "DTF_FUN ( M , True , 'D0' )" ,
                    "LOKI_DTF_MASS_Dp"               : "DTF_FUN ( M , True , 'D+' )" ,
                    "LOKI_DTF_MASS_Dst"              : "DTF_FUN ( M , True , 'D*(2010)+' )" ,                    
                    "LOKI_MASS_DstDConstr"           : "DTF_FUN ( M , True , strings ( ['D*(2010)-','D+', 'D*(2010)+','D-'] ) )" ,
                    "LOKI_MASSERR_DstDConstr"        : "sqrt(DTF_FUN ( M2ERR2 , True , strings(['D*(2010)-','D+', 'D*(2010)+','D-'])))",
                    "LOKI_Rho_Dist"                  : "BPVVDR",
                    "LOKI_CORRM"                     : "BPVCORRM",
                    "LOKI_Costheta_star1"            : "LV01",
                    "LOKI_DOCA"                      : "DOCA(1,2)",
                    "LOKI_FDS"                       : "BPVDLS" ,
                   # "LOKI_ptasy_1.50"                : "RELINFO('/Event/Bhadron/Phys/B02DstDD02K3PiBeauty2CharmLine/P2ConeVar1','CONEPTASYM',-1000.)",
                    "LOKI_COSPOL_D0"                 : "COSPOL( '[([B0]cc ->  ( D*(2010)- ->  pi- ^( [D~0]cc ->  K+  (K- || pi-)))  (D+ ->  K-  (K+ || pi+)  pi+))]CC', '[(Beauty -> ^( D*(2010)- ->  pi-  ( [D~0]cc ->  K+  (K- || pi-)))  (D+ ->  K-  (K+ || pi+)  pi+))]CC' )",
                    "LOKI_COSPOL_pi_D0"              : "COSPOL( '[([B0]cc ->  ( D*(2010)- ->  pi-  ( [D~0]cc ->  K+ ^(K- || pi-)))  (D+ ->  K-  (K+ || pi+)  pi+))]CC', '[(Beauty ->  ( D*(2010)- ->  pi- ^( [D~0]cc ->  K+  (K- || pi-)))  (D+ ->  K-  (K+ || pi+)  pi+))]CC' )",
                    "LOKI_COSPOL_K_D0"               : "COSPOL( '[([B0]cc ->  ( D*(2010)- ->  pi-  ( [D~0]cc -> ^K+  (K- || pi-)))  (D+ ->  K-  (K+ || pi+)  pi+))]CC', '[(Beauty ->  ( D*(2010)- ->  pi- ^( [D~0]cc ->  K+  (K- || pi-)))  (D+ ->  K-  (K+ || pi+)  pi+))]CC' )",
                    "LOKI_COSPOL_slowpi"             : "COSPOL( '[([B0]cc ->  ( D*(2010)- -> ^pi-  ( [D~0]cc ->  K+  (K- || pi-)))  (D+ ->  K-  (K+ || pi+)  pi+))]CC', '[(Beauty -> ^( D*(2010)- ->  pi-  ( [D~0]cc ->  K+  (K- || pi-)))  (D+ ->  K-  (K+ || pi+)  pi+))]CC' )",
                    "LOKI_COSPOL_K_D"                : "COSPOL( '[([B0]cc ->  ( D*(2010)- ->  pi-  ( [D~0]cc ->  K+  (K- || pi-)))  (D+ -> ^K-  (K+ || pi+)  pi+))]CC', '[(Beauty ->  ( D*(2010)- ->  pi-  ( [D~0]cc ->  K+  (K- || pi-))) ^(D+ ->  K-  (K+ || pi+)  pi+))]CC' )",
                    "LOKI_COSPOL_pi_D"               : "COSPOL( '[([B0]cc ->  ( D*(2010)- ->  pi-  ( [D~0]cc ->  K+  (K- || pi-)))  (D+ ->  K-  (K+ || pi+) ^pi+))]CC', '[(Beauty ->  ( D*(2010)- ->  pi-  ( [D~0]cc ->  K+  (K- || pi-))) ^(D+ ->  K-  (K+ || pi+)  pi+))]CC' )",
                    "LOKI_COSPOL_pi_or_K_D"          : "COSPOL( '[([B0]cc ->  ( D*(2010)- ->  pi-  ( [D~0]cc ->  K+  (K- || pi-)))  (D+ ->  K- ^(K+ || pi+)  pi+))]CC', '[(Beauty ->  ( D*(2010)- ->  pi-  ( [D~0]cc ->  K+  (K- || pi-))) ^(D+ ->  K-  (K+ || pi+)  pi+))]CC' )"
                     }

tt_loki_B = tuple.__getattr__("B0").addTupleTool("LoKi::Hybrid::TupleTool/tt_loki_B")
tt_loki_B.Variables = loki_variables_B

tuple.addTool(TupleToolDecay, name="Dstar")
tuple.addTool(TupleToolDecay, name="D0")
tuple.addTool(TupleToolDecay, name="D")

LoKi_Dstar=LoKi__Hybrid__TupleTool("LoKi_Dstar")
LoKi_Dstar.Variables =  {
    "LOKI_MASS_D0"        : "DTF_FUN ( M , True , 'D0' )" ,
    "DTF_CTAU"            : "DTF_CTAU( 0, True )",
    "LOKI_DTF_VCHI2NDOF"  : "DTF_FUN ( VFASPF(VCHI2/VDOF) , True  )",
    "Dist_DTF_z"          : "DTF_FUN ( BPVVDR , True , 'D0')",
    "Dist_z"              : "BPVVDR",
    "CORRM"               : "BPVCORRM",
    "Costheta_star1"      : "LV01",
    "Costheta_star2"      : "LV02",
    "DOCA"                : "DOCA(1,2)"
    }
tuple.Dstar.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_Dstar"]
tuple.Dstar.addTool(LoKi_Dstar)

LoKi_D0=LoKi__Hybrid__TupleTool("LoKi_D0")
LoKi_D0.Variables =  {
    "DTF_CTAUS"       : "DTF_CTAUSIGNIFICANCE( 0, True )",
    "DTF_CTAU"        : "DTF_CTAU( 0, True )",
    "DTF_CHI2NDOF"    : "DTF_CHI2NDOF( True )",
    "FDS"             : "BPVDLS",
    "Dist_z"          : "BPVVDR",
    "Costheta_star0"  : "LV01",
    "CORRM"           : "BPVCORRM",
    "Costheta_star2"  : "LV02",
    "DOCA"            : "DOCA(1,2)",
    "M12"             : "MASS(1,2)",
    "M14"             : "MASS(1,4)",
    "M23"             : "MASS(2,3)",
    "M34"             : "MASS(3,4)"
    }
tuple.D0.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_D0"]
tuple.D0.addTool(LoKi_D0)

LoKi_Dplus=LoKi__Hybrid__TupleTool("LoKi_Dplus")
LoKi_Dplus.Variables =  {
    "DTF_CTAUS"       : "DTF_CTAUSIGNIFICANCE( 0, True )",
    "DTF_CTAU"        : "DTF_CTAU( 0, True )",
    "DTF_CHI2NDOF"    : "DTF_CHI2NDOF( True )",
    "FDS"             : "BPVDLS",
    "Inv_mass_12"     : "M12",
    "Inv_mass_13"     : "M13",
    "Inv_mass_23"     : "M23",
    "Dist_z"          : "BPVVDR",
    "Dist_DTF_z"      : "DTF_FUN(BPVVDR ,True , 'D0')",
    "Lc_mass1"        : "WM('K-' ,'p+'  , 'pi+')" ,
    "Lc_mass2"        : "WM('K-' ,'pi+' , 'p+')" ,
    "Ds_mass1"        : "WM('K-' ,'K+'  , 'pi+')" ,
    "Ds_mass2"        : "WM('K-' ,'pi+'  , 'K+')" ,
    "phi_mass"        : "WM('K-' ,'K+')",
    "Costheta_star0"  : "LV01",
    "Costheta_star2"  : "LV02",
    "Costheta_star3"  : "LV03",
    "DOCA1"           : "DOCA(1,2)",
    "DOCA2"           : "DOCA(3,2)",
    "DOCA3"           : "DOCA(1,3)",
    "CORRM"           : "BPVCORRM"
    }
tuple.D.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_Dplus"]
tuple.D.addTool(LoKi_Dplus)
######################################
TupleToolTISTOS = TupleToolTISTOS('TupleToolTISTOS')
TupleToolTISTOS.TriggerList= trigger_lines
TupleToolTISTOS.VerboseL0 = True
TupleToolTISTOS.VerboseHlt1 = True
TupleToolTISTOS.VerboseHlt2 = True
tuple.B0.addTool(TupleToolTISTOS , name = 'TupleToolTISTOS' )
tuple.B0.ToolList   += [ "TupleToolTISTOS"]
##############################################################################################
##### CONSTRAINTS
##############################################################################################
#No constraints at all
Fit = tuple.__getattr__("B0").addTupleTool("TupleToolDecayTreeFitterExtended/Fit")
Fit.Verbose = True
#Fit.OutputLevel = VERBOSE
Fit.UpdateDaughters = True
Fit.constrainToOriginVertex = False

#Constraint only on PV 
FitPVConst = tuple.__getattr__("B0").addTupleTool("TupleToolDecayTreeFitterExtended/FitPVConst")
FitPVConst.Verbose = True
#FitPVConst.OutputLevel = VERBOSE
FitPVConst.UpdateDaughters = True
FitPVConst.constrainToOriginVertex = True

#Constraint on daughters but not on PV
FitDstDConst = tuple.__getattr__("B0").addTupleTool("TupleToolDecayTreeFitterExtended/FitDstDConst")
FitDstDConst.Verbose = True
#FitDstDConst.OutputLevel = VERBOSE
FitDstDConst.UpdateDaughters = True
FitDstDConst.daughtersToConstrain = ["D*(2010)-","D+","D*(2010)+","D-"]
FitDstDConst.constrainToOriginVertex = False

#Constraint on daughters and constraint to OriginPV
FitDstDPVConst = tuple.__getattr__("B0").addTupleTool("TupleToolDecayTreeFitterExtended/FitDstDPVConst")
FitDstDPVConst.Verbose = True
#FitDstDPVConst.OutputLevel = VERBOSE
FitDstDPVConst.UpdateDaughters = True
FitDstDPVConst.daughtersToConstrain = ["D*(2010)-","D+","D*(2010)+","D-"]
FitDstDPVConst.constrainToOriginVertex = True

#Constraint on daughters but not on PV
FitDstDDConst = tuple.__getattr__("B0").addTupleTool("TupleToolDecayTreeFitterExtended/FitDstDDConst")
FitDstDDConst.Verbose = True
#FitDstDDConst.OutputLevel = VERBOSE
FitDstDDConst.UpdateDaughters = True
FitDstDDConst.daughtersToConstrain = ["D*(2010)-","D+", "D0","D*(2010)+","D-", "D~0"]
FitDstDDConst.constrainToOriginVertex = False

#Constraint on daughters and constraint to OriginPV
FitDstDDPVConst = tuple.__getattr__("B0").addTupleTool("TupleToolDecayTreeFitterExtended/FitDstDDPVConst")
FitDstDDPVConst.Verbose = True
#FitDstDDPVConst.OutputLevel = VERBOSE
FitDstDDPVConst.UpdateDaughters = True
FitDstDDPVConst.daughtersToConstrain = ["D*(2010)-","D+", "D0","D*(2010)+","D-", "D~0"]
FitDstDDPVConst.constrainToOriginVertex = True

#Constraint on daughters but not on PV
FitDstDDsConst = tuple.__getattr__("B0").addTupleTool("TupleToolDecayTreeFitterExtended/FitDstDDsConst")
FitDstDDsConst.Verbose = True
#FitDstDDsConst.OutputLevel = VERBOSE
FitDstDDsConst.UpdateDaughters = True
FitDstDDsConst.daughtersToConstrain = ["D*(2010)-","D_s+", "D0","D*(2010)+","D_s-", "D~0"]
FitDstDDsConst.constrainToOriginVertex = False
FitDstDDsConst.Substitutions =  { "[([B0]cc ->  ( D*(2010)- ->  pi- ( [D~0]cc ->  K+  (K- || pi-)))  ^(D+ ->  K-  (K+ || pi+)  pi+))]CC" : "D_s+" }

#Constraint on daughters and constraint to OriginPV
FitDstDDsPVConst = tuple.__getattr__("B0").addTupleTool("TupleToolDecayTreeFitterExtended/FitDstDDsPVConst")
FitDstDDsPVConst.Verbose = True
#FitDstDDsPVConst.OutputLevel = VERBOSE
FitDstDDsPVConst.UpdateDaughters = True
FitDstDDsPVConst.daughtersToConstrain = ["D*(2010)-","D_s+", "D0","D*(2010)+","D_s-", "D~0"]
FitDstDDsPVConst.constrainToOriginVertex = True
FitDstDDsPVConst.Substitutions =  { "[([B0]cc ->  ( D*(2010)- ->  pi- ( [D~0]cc ->  K+  (K- || pi-)))  ^(D+ ->  K-  (K+ || pi+)  pi+))]CC" : "D_s+" }

#Constraint on Dst and Ds but not on PV
FitDstDsConst = tuple.__getattr__("B0").addTupleTool("TupleToolDecayTreeFitterExtended/FitDstDsConst")
FitDstDsConst.Verbose = True
#FitDstDsConst.OutputLevel = VERBOSE
FitDstDsConst.UpdateDaughters = True
FitDstDsConst.daughtersToConstrain = ["D_s+","D*(2010)+","D_s-","D*(2010)-"]
FitDstDsConst.constrainToOriginVertex = False
FitDstDsConst.Substitutions =  { "[([B0]cc ->  ( D*(2010)- ->  pi- ( [D~0]cc ->  K+  (K- || pi-)))  ^(D+ ->  K-  (K+ || pi+)  pi+))]CC" : "D_s+" }

#Constraint on Dst, Ds and on PV
FitDstDsPVConst = tuple.__getattr__("B0").addTupleTool("TupleToolDecayTreeFitterExtended/FitDstDsPVConst")
FitDstDsPVConst.Verbose = True
#FitDstDsPVConst.OutputLevel = VERBOSE
FitDstDsPVConst.UpdateDaughters = True
FitDstDsPVConst.daughtersToConstrain = ["D_s+","D*(2010)+","D_s-","D*(2010)-"]
FitDstDsPVConst.constrainToOriginVertex = True
FitDstDsPVConst.Substitutions =  { "[([B0]cc ->  ( D*(2010)- ->  pi- ( [D~0]cc ->  K+  (K- || pi-)))  ^(D+ ->  K-  (K+ || pi+)  pi+))]CC" : "D_s+" }

#Constraint on Dst and D0 but not on PV
FitDstD0Const = tuple.__getattr__("B0").addTupleTool("TupleToolDecayTreeFitterExtended/FitDstD0Const")
FitDstD0Const.Verbose = True
#FitDstD0Const.OutputLevel = VERBOSE
FitDstD0Const.UpdateDaughters = True
FitDstD0Const.daughtersToConstrain = ["D0","D*(2010)+","D~0","D*(2010)-"]
FitDstD0Const.constrainToOriginVertex = False

#Constraint on Dst, D0 and on PV
FitDstD0PVConst = tuple.__getattr__("B0").addTupleTool("TupleToolDecayTreeFitterExtended/FitDstD0PVConst")
FitDstD0PVConst.Verbose = True
#FitDstD0PVConst.OutputLevel = VERBOSE
FitDstD0PVConst.UpdateDaughters = True
FitDstD0PVConst.daughtersToConstrain = ["D0","D*(2010)+","D~0","D*(2010)-"]
FitDstD0PVConst.constrainToOriginVertex = True

#Constraint on D and D0 but not on PV
FitD0DConst = tuple.__getattr__("B0").addTupleTool("TupleToolDecayTreeFitterExtended/FitD0DConst")
FitD0DConst.Verbose = True
#FitD0DConst.OutputLevel = VERBOSE
FitD0DConst.UpdateDaughters = True
FitD0DConst.daughtersToConstrain = ["D0","D+","D~0","D-"]
FitD0DConst.constrainToOriginVertex = False

#Constraint on D, D0 and on PV
FitD0DPVConst = tuple.__getattr__("B0").addTupleTool("TupleToolDecayTreeFitterExtended/FitD0DPVConst")
FitD0DPVConst.Verbose = True
#FitD0DPVConst.OutputLevel = VERBOSE
FitD0DPVConst.UpdateDaughters = True
FitD0DPVConst.daughtersToConstrain = ["D0","D+","D~0","D-"]
FitD0DPVConst.constrainToOriginVertex = True

#Constraint on Ds and D0 but not on PV
FitD0DsConst = tuple.__getattr__("B0").addTupleTool("TupleToolDecayTreeFitterExtended/FitD0DsConst")
FitD0DsConst.Verbose = True
#FitD0DsConst.OutputLevel = VERBOSE
FitD0DsConst.UpdateDaughters = True
FitD0DsConst.daughtersToConstrain = ["D0","D_s+","D~0","D_s-"]
FitD0DsConst.constrainToOriginVertex = False
FitD0DsConst.Substitutions =  { "[([B0]cc ->  ( D*(2010)- ->  pi- ( [D~0]cc ->  K+  (K- || pi-)))  ^(D+ ->  K-  (K+ || pi+)  pi+))]CC" : "D_s+" }

#Constraint on Ds, D0 and on PV
FitD0DsPVConst = tuple.__getattr__("B0").addTupleTool("TupleToolDecayTreeFitterExtended/FitD0DsPVConst")
FitD0DsPVConst.Verbose = True
#FitD0DsPVConst.OutputLevel = VERBOSE
FitD0DsPVConst.UpdateDaughters = True
FitD0DsPVConst.daughtersToConstrain = ["D0","D_s+","D~0","D_s-"]
FitD0DsPVConst.constrainToOriginVertex = True
FitD0DsPVConst.Substitutions =  { "[([B0]cc ->  ( D*(2010)- ->  pi- ( [D~0]cc ->  K+  (K- || pi-)))  ^(D+ ->  K-  (K+ || pi+)  pi+))]CC" : "D_s+" }

#Constraint on D
FitDConst = tuple.__getattr__("B0").addTupleTool("TupleToolDecayTreeFitterExtended/FitDConst")
FitDConst.Verbose = True
#FitDConst.OutputLevel = VERBOSE
FitDConst.UpdateDaughters = True
FitDConst.daughtersToConstrain = ["D+","D-"]
FitDConst.constrainToOriginVertex = False

#Constraint on D and on PV
FitDPVConst = tuple.__getattr__("B0").addTupleTool("TupleToolDecayTreeFitterExtended/FitDPVConst")
FitDPVConst.Verbose = True
#FitDPVConst.OutputLevel = VERBOSE
FitDPVConst.UpdateDaughters = True
FitDPVConst.daughtersToConstrain = ["D+","D-"]
FitDPVConst.constrainToOriginVertex = True

#Constraint on Ds
FitDsConst = tuple.__getattr__("B0").addTupleTool("TupleToolDecayTreeFitterExtended/FitDsConst")
FitDsConst.Verbose = True
#FitDsConst.OutputLevel = VERBOSE
FitDsConst.UpdateDaughters = True
FitDsConst.daughtersToConstrain = ["D_s+","D_s-"]
FitDsConst.constrainToOriginVertex = False
FitDsConst.Substitutions =  { "[([B0]cc ->  ( D*(2010)- ->  pi- ( [D~0]cc ->  K+  (K- || pi-)))  ^(D+ ->  K-  (K+ || pi+)  pi+))]CC" : "D_s+" }

#Constraint on Ds and on PV
FitDsPVConst = tuple.__getattr__("B0").addTupleTool("TupleToolDecayTreeFitterExtended/FitDsPVConst")
FitDsPVConst.Verbose = True
#FitDsPVConst.OutputLevel = VERBOSE
FitDsPVConst.UpdateDaughters = True
FitDsPVConst.daughtersToConstrain = ["D_s+","D_s-"]
FitDsPVConst.constrainToOriginVertex = True
FitDsPVConst.Substitutions =  { "[([B0]cc ->  ( D*(2010)- ->  pi- ( [D~0]cc ->  K+  (K- || pi-)))  ^(D+ ->  K-  (K+ || pi+)  pi+))]CC" : "D_s+" }

#Constraint on D0
FitD0Const = tuple.__getattr__("B0").addTupleTool("TupleToolDecayTreeFitterExtended/FitD0Const")
FitD0Const.Verbose = True
#FitD0Const.OutputLevel = VERBOSE
FitD0Const.UpdateDaughters = True
FitD0Const.daughtersToConstrain = ["D0","D~0"]
FitD0Const.constrainToOriginVertex = False

#Constraint on D0 and on PV
FitD0PVConst = tuple.__getattr__("B0").addTupleTool("TupleToolDecayTreeFitterExtended/FitD0PVConst")
FitD0PVConst.Verbose = True
#FitD0PVConst.OutputLevel = VERBOSE
FitD0PVConst.UpdateDaughters = True
FitD0PVConst.daughtersToConstrain = ["D0","D~0"]
FitD0PVConst.constrainToOriginVertex = True

#Constraint on Dst
FitDstConst = tuple.__getattr__("B0").addTupleTool("TupleToolDecayTreeFitterExtended/FitDstConst")
FitDstConst.Verbose = True
#FitDstConst.OutputLevel = VERBOSE
FitDstConst.UpdateDaughters = True
FitDstConst.daughtersToConstrain = ["D*(2010)+","D*(2010)-"]
FitDstConst.constrainToOriginVertex = False

#Constraint on Dst and on PV
FitDstPVConst = tuple.__getattr__("B0").addTupleTool("TupleToolDecayTreeFitterExtended/FitDstPVConst")
FitDstPVConst.Verbose = True
#FitDstPVConst.OutputLevel = VERBOSE
FitDstPVConst.UpdateDaughters = True
FitDstPVConst.daughtersToConstrain = ["D*(2010)+","D*(2010)-"]
FitDstPVConst.constrainToOriginVertex = True
###########################################################
#### D0 K pi pi pi :
# Filter the tuple :                                                                                                                                           
B02DstD_K3pi_InputLocation = prefix + "Phys/B02DstDD02K3PiBeauty2CharmLine/Particles" 
B02DstD_Kpi_InputLocation  = "Phys/B02DstDBeauty2CharmLine/Particles" 
 
tuple_D0K3pi = DecayTreeTuple("D02K3piDKpipi") 
tuple_D0K3pi.Inputs = [B02DstD_K3pi_InputLocation ] 
tuple_D0K3pi.Decay = "[ [B0]cc -> ^( D*(2010)- -> ^pi- ^( [D~0]cc -> ^K+ ^pi- ^pi+ ^pi-) ) ^(D+ -> ^K- ^pi+ ^pi+) ]CC" 
tuple_D0K3pi.ToolList +=  [ 
    "TupleToolGeometry" 
    ,"TupleToolKinematic" 
    , "TupleToolPrimaries" 
    , "TupleToolTrackInfo" 
    , "TupleToolRecoStats" 
    , "TupleToolTrackPosition" 
    , "LoKi::Hybrid::TupleTool/LoKiTool" 
    ] 
tuple_D0K3pi.ReFitPVs = True 
tuple_D0K3pi.OutputLevel = 3 
 
tuple_D0K3pi.Branches = { 
     "slowpi" : "[ [B0]cc ->  ( D*(2010)- ->  ^pi-  ( [D~0]cc ->  (pi-|| K-)  (pi-|| K-)  (pi+ || K+)  (pi+ || K+) ) )  ( D+ ->  (K-|| pi-)  (pi+||K+)  (pi+||K+) ) ]CC" 
    ,"pi1_D0" : "[ [B0]cc ->  ( D*(2010)- ->   pi-  ( [D~0]cc -> ^(pi-|| K-)  (pi-|| K-)  (pi+ || K+)  (pi+ || K+) ) )  ( D+ ->  (K-|| pi-)  (pi+||K+)  (pi+||K+) ) ]CC" 
    ,"pi2_D0" : "[ [B0]cc ->  ( D*(2010)- ->   pi-  ( [D~0]cc ->  (pi-|| K-) ^(pi-|| K-)  (pi+ || K+)  (pi+ || K+) ) )  ( D+ ->  (K-|| pi-)  (pi+||K+)  (pi+||K+) ) ]CC" 
    ,"pi3_D0" : "[ [B0]cc ->  ( D*(2010)- ->   pi-  ( [D~0]cc ->  (pi-|| K-)  (pi-|| K-) ^(pi+ || K+)  (pi+ || K+) ) )  ( D+ ->  (K-|| pi-)  (pi+||K+)  (pi+||K+) ) ]CC" 
    ,"K_D0"   : "[ [B0]cc ->  ( D*(2010)- ->   pi-  ( [D~0]cc ->  (pi-|| K-)  (pi-|| K-)  (pi+ || K+) ^(pi+ || K+) ) )  ( D+ ->  (K-|| pi-)  (pi+||K+)  (pi+||K+) ) ]CC" 
    ,"D0"     : "[ [B0]cc ->  ( D*(2010)- ->   pi- ^( [D~0]cc ->  (pi-|| K-)  (pi-|| K-)  (pi+ || K+)  (pi+ || K+) ) )  ( D+ ->  (K-|| pi-)  (pi+||K+)  (pi+||K+) ) ]CC" 
    ,"Dstar"  : "[ [B0]cc -> ^( D*(2010)- ->   pi-  ( [D~0]cc ->  (pi-|| K-)  (pi-|| K-)  (pi+ || K+)  (pi+ || K+) ) )  ( D+ ->  (K-|| pi-)  (pi+||K+)  (pi+||K+) ) ]CC" 
    ,"pi1_Dp" : "[ [B0]cc ->  ( D*(2010)- ->   pi-  ( [D~0]cc ->  (pi-|| K-)  (pi-|| K-)  (pi+ || K+)  (pi+ || K+) ) )  ( D+ ->  (K-|| pi-)  (pi+||K+) ^(pi+||K+) ) ]CC" 
    ,"pi2_Dp" : "[ [B0]cc ->  ( D*(2010)- ->   pi-  ( [D~0]cc ->  (pi-|| K-)  (pi-|| K-)  (pi+ || K+)  (pi+ || K+) ) )  ( D+ ->  (K-|| pi-) ^(pi+||K+)  (pi+||K+) ) ]CC" 
    ,"K_Dp"   : "[ [B0]cc ->  ( D*(2010)- ->   pi-  ( [D~0]cc ->  (pi-|| K-)  (pi-|| K-)  (pi+ || K+)  (pi+ || K+) ) )  ( D+ -> ^(K-|| pi-)  (pi+||K+)  (pi+||K+) ) ]CC" 
    ,"Dp"     : "[ [B0]cc ->  ( D*(2010)- ->   pi-  ( [D~0]cc ->  (pi-|| K-)  (pi-|| K-)  (pi+ || K+)  (pi+ || K+) ) ) ^( D+ ->  (K-|| pi-)  (pi+||K+)  (pi+||K+) ) ]CC" 
    ,"B0"     : "[ [B0]cc ->  ( D*(2010)- ->   pi-  ( [D~0]cc ->  (pi-|| K-)  (pi-|| K-)  (pi+ || K+)  (pi+ || K+) ) )  ( D+ ->  (K-|| pi-)  (pi+||K+)  (pi+||K+) ) ]CC" 
    } 
tuple_D0K3pi.addTool(TupleToolDecay, name="B0") 
######################################    
LoKi_B0=LoKi__Hybrid__TupleTool("LoKi_B0") 
LoKi_B0.Variables =  { 
    "ETA"             : "ETA", 
    "PHI"             : "PHI", 
    "DTF_TAU"         : "DTF_CTAU( 0, True )/0.299792458", 
    "DTF_CTAUS"       : "DTF_CTAUSIGNIFICANCE( 0, True )", 
    "DTF_CHI2NDOF"    : "DTF_CHI2NDOF( True )", 
    "DTF_MASS_3D"     : "DTF_FUN ( M , True , strings( ['D*(2010)+', 'D0' , 'D+'] ) )" , 
    "DTF_MASS_3DDs"   : "DTF_FUN ( M , True , strings( ['D*(2010)+', 'D0' , 'D_s+'] ) )" , 
    "DTF_VCHI2NDOF_3D": "DTF_FUN ( VFASPF(VCHI2/VDOF) , True , strings( ['D*(2010)+', 'D0' , 'D+'] ) )" , 
    "DTF_VCHI2NDOF_3D_Ds": "DTF_FUN ( VFASPF(VCHI2/VDOF) , True , strings( ['D*(2010)+', 'D0' , 'D_s+'] ) )" , 
    "DTF_MASS_DstD0"  : "DTF_FUN ( M , True , strings( ['D*(2010)+', 'D0'] ))" , 
    "DTF_MASS_DstDp"  : "DTF_FUN ( M , True , strings( ['D*(2010)+', 'D+'] ))" , 
    "DTF_MASS_DstDs"  : "DTF_FUN ( M , True , strings( ['D*(2010)+', 'D_s+'] ))" , 
    "DTF_MASS_D0Dp"   : "DTF_FUN ( M , True , strings( ['D0'      , 'D+'] ))" , 
    "DTF_MASS_D0Ds"   : "DTF_FUN ( M , True , strings( ['D0'      , 'D_s+'] ))" , 
    "DTF_MASS_D0"     : "DTF_FUN ( M , True , 'D0' )" , 
    "DTF_MASS_Dp"     : "DTF_FUN ( M , True , 'D+' )" , 
    "DTF_MASS_Dst"    : "DTF_FUN ( M , True , 'D*(2010)+' )" , 
    "DTF_VCHI2NDOF"   : "DTF_FUN ( VFASPF(VCHI2/VDOF) , True  )", 
    "DTF_VtxZ"        : "DTF_FUN ( BPVVDR , True ,strings( ['D*(2010)+', 'D0' , 'D+'] ) )", 
    "Rho_Dist"        : "BPVVDR", 
    "CORRM"           : "BPVCORRM", 
    "Costheta_star1"  : "LV01", 
    "DOCA"            : "DOCA(1,2)", 
    "DTF_CTAU"        : "DTF_CTAU( 0, True )", 
    "FDS"             : "BPVDLS" , 
    "ptasy_1.50"      : "RELINFO('/Event/BhadronCompleteEvent/Phys/B02DstDD02K3PiBeauty2CharmLine/P2ConeVar1','CONEPTASYM',-1000.)", 
    "COSPOL_D0"       : "COSPOL( '[[B0]cc -> (D*(2010)- ->  pi- ^([D~0]cc -> K+ pi- pi+ pi-)) (D+ -> K- pi+ pi+)]CC','[Beauty -> ^(D*(2010)- -> pi- ([D~0]cc -> K+ pi- pi+ pi-)) (D+ -> K- pi+ pi+)]CC' )", 
    "COSPOL_pi1_D0"   : "COSPOL( '[[B0]cc -> (D*(2010)- -> pi- ([D~0]cc -> K+ ^pi- pi+ pi-)) (D+ -> K- pi+ pi+)]CC','[Beauty -> (D*(2010)- -> pi- ^([D~0]cc -> K+ pi- pi+ pi-)) (D+ -> K- pi+ pi+)]CC' ) ", 
    "COSPOL_pi2_D0"   : "COSPOL( '[[B0]cc -> (D*(2010)- ->  pi-  ([D~0]cc -> K+ pi- ^pi+ pi-)) (D+ ->  K-  pi+  pi+)]CC' ,'[Beauty ->  (D*(2010)- -> pi- ^([D~0]cc -> K+ pi- pi+ pi-))  (D+ -> K- pi+ pi+)]CC' ) ", 
    "COSPOL_pi3_D0"   : "COSPOL( '[[B0]cc -> (D*(2010)- ->  pi-  ([D~0]cc -> K+ pi- pi+ ^pi-)) (D+ ->  K-  pi+  pi+)]CC' ,'[Beauty ->  (D*(2010)- -> pi- ^([D~0]cc -> K+ pi- pi+ pi-))  (D+ -> K- pi+ pi+)]CC' ) ", 
    "COSPOL_pi_Dst"   : "COSPOL( '[[B0]cc -> (D*(2010)- -> ^pi-  ([D~0]cc -> K+ pi- pi+ pi-)) (D+ ->  K-  pi+  pi+)]CC' ,'[Beauty -> ^(D*(2010)- -> pi-  ([D~0]cc -> K+ pi- pi+ pi-))  (D+ -> K- pi+ pi+)]CC' )", 
    "COSPOL_K_Dp"     : "COSPOL( '[[B0]cc -> (D*(2010)- ->  pi- ([D~0]cc -> K+ pi- pi+ pi-)) (D+ -> ^K-  pi+  pi+)]CC' ,'[Beauty ->  (D*(2010)- -> pi-  ([D~0]cc -> K+ pi- pi+ pi-)) ^(D+ -> K- pi+ pi+)]CC' ) ", 
    "COSPOL_pim_Dp"   : "COSPOL( '[[B0]cc -> (D*(2010)- ->  pi-  ([D~0]cc -> K+ pi- pi+ pi-)) (D+ ->  K-  pi+ ^pi+)]CC' ,'[Beauty ->  (D*(2010)- -> pi-  ([D~0]cc -> K+ pi- pi+ pi-)) ^(D+ -> K- pi+ pi+)]CC' ) ", 
    "COSPOL_pip_Dp"   : "COSPOL( '[[B0]cc -> (D*(2010)- ->  pi-  ([D~0]cc -> K+ pi- pi+ pi-)) (D+ ->  K- ^pi+  pi+)]CC' ,'[Beauty ->  (D*(2010)- -> pi- ([D~0]cc -> K+ pi- pi+ pi-)) ^(D+ -> K- pi+ pi+)]CC' )" 
}     
tuple_D0K3pi.B0.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_B0"] 
tuple_D0K3pi.B0.addTool(LoKi_B0) 

tuple_D0K3pi.addTool(TupleToolDecay, name="Dstar") 
tuple_D0K3pi.addTool(TupleToolDecay, name="D0") 
tuple_D0K3pi.addTool(TupleToolDecay, name="Dp") 

tt_tagging3 = tuple_D0K3pi.addTupleTool("TupleToolTagging")
tt_tagging3.Verbose = True
tt_tagging3.AddMVAFeatureInfo = False
tt_tagging3.AddTagPartsInfo = False
tt_tagging3.OutputLevel = INFO
btagtool3 = tt_tagging3.addTool(BTaggingTool, name = "MyBTaggingTool3")
applyFTTuning(btagtool3, tuning_version= OptimizationVersion)
tt_tagging3.TaggingToolName = btagtool3.getFullName()
tt_tagging3.Verbose = True

tuple_D0K3pi.Dstar.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_Dstar"] 
tuple_D0K3pi.Dstar.addTool(LoKi_Dstar) 
 
tuple_D0K3pi.D0.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_D0"] 
tuple_D0K3pi.D0.addTool(LoKi_D0) 
 
tuple_D0K3pi.Dp.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_Dplus"] 
tuple_D0K3pi.Dp.addTool(LoKi_Dplus) 

tuple_D0K3pi.B0.addTool(TupleToolTISTOS , name = 'TupleToolTISTOS' ) 
tuple_D0K3pi.B0.ToolList   += [ "TupleToolTISTOS"] 
###################################################################### 
tuple_D02K3piDKKpi        =  tuple_D0K3pi.clone("D02K3piDKKpi") 
tuple_D02K3piDKKpi.Decay  = "[ [B0]cc -> ^( D*(2010)- -> ^pi- ^( [D~0]cc -> ^K+ ^pi- ^pi+ ^pi-) ) ^(D+ -> ^K- ^K+ ^pi+) ]CC" 
tuple_D02K3piDKKpi.Inputs =  [B02DstD_K3pi_InputLocation] 
tuple_D02K3piDKKpi.Branches = { 
    "slowpi" : "[ [B0]cc ->  ( D*(2010)- ->  ^pi-  ( [D~0]cc ->  (pi-|| K-)  (pi-|| K-)  (pi+ || K+)  (pi+ || K+) ) )  ( D+ ->  (K-|| pi-)  (pi+||K+)  (pi+||K+) ) ]CC" 
    ,"pi1_D0" : "[ [B0]cc ->  ( D*(2010)- ->   pi-  ( [D~0]cc -> ^(pi-|| K-)  (pi-|| K-)  (pi+ || K+)  (pi+ || K+) ) )  ( D+ ->  (K-|| pi-)  (pi+||K+)  (pi+||K+) ) ]CC" 
    ,"pi2_D0" : "[ [B0]cc ->  ( D*(2010)- ->   pi-  ( [D~0]cc ->  (pi-|| K-) ^(pi-|| K-)  (pi+ || K+)  (pi+ || K+) ) )  ( D+ ->  (K-|| pi-)  (pi+||K+)  (pi+||K+) ) ]CC" 
    ,"pi3_D0" : "[ [B0]cc ->  ( D*(2010)- ->   pi-  ( [D~0]cc ->  (pi-|| K-)  (pi-|| K-) ^(pi+ || K+)  (pi+ || K+) ) )  ( D+ ->  (K-|| pi-)  (pi+||K+)  (pi+||K+) ) ]CC" 
    ,"K_D0"   : "[ [B0]cc ->  ( D*(2010)- ->   pi-  ( [D~0]cc ->  (pi-|| K-)  (pi-|| K-)  (pi+ || K+) ^(pi+ || K+) ) )  ( D+ ->  (K-|| pi-)  (pi+||K+)  (pi+||K+) ) ]CC" 
    ,"D0"     : "[ [B0]cc ->  ( D*(2010)- ->   pi- ^( [D~0]cc ->  (pi-|| K-)  (pi-|| K-)  (pi+ || K+)  (pi+ || K+) ) )  ( D+ ->  (K-|| pi-)  (pi+||K+)  (pi+||K+) ) ]CC" 
    ,"Dstar"  : "[ [B0]cc -> ^( D*(2010)- ->   pi-  ( [D~0]cc ->  (pi-|| K-)  (pi-|| K-)  (pi+ || K+)  (pi+ || K+) ) )  ( D+ ->  (K-|| pi-)  (pi+||K+)  (pi+||K+) ) ]CC" 
    ,"pi1_Dp" : "[ [B0]cc ->  ( D*(2010)- ->   pi-  ( [D~0]cc ->  (pi-|| K-)  (pi-|| K-)  (pi+ || K+)  (pi+ || K+) ) )  ( D+ ->  (K-|| pi-)  (pi+||K+) ^(pi+||K+) ) ]CC" 
    ,"pi2_Dp" : "[ [B0]cc ->  ( D*(2010)- ->   pi-  ( [D~0]cc ->  (pi-|| K-)  (pi-|| K-)  (pi+ || K+)  (pi+ || K+) ) )  ( D+ ->  (K-|| pi-) ^(pi+||K+)  (pi+||K+) ) ]CC" 
    ,"K_Dp"   : "[ [B0]cc ->  ( D*(2010)- ->   pi-  ( [D~0]cc ->  (pi-|| K-)  (pi-|| K-)  (pi+ || K+)  (pi+ || K+) ) )  ( D+ -> ^(K-|| pi-)  (pi+||K+)  (pi+||K+) ) ]CC" 
    ,"Dp"     : "[ [B0]cc ->  ( D*(2010)- ->   pi-  ( [D~0]cc ->  (pi-|| K-)  (pi-|| K-)  (pi+ || K+)  (pi+ || K+) ) ) ^( D+ ->  (K-|| pi-)  (pi+||K+)  (pi+||K+) ) ]CC" 
    ,"B0"     : "[ [B0]cc ->  ( D*(2010)- ->   pi-  ( [D~0]cc ->  (pi-|| K-)  (pi-|| K-)  (pi+ || K+)  (pi+ || K+) ) )  ( D+ ->  (K-|| pi-)  (pi+||K+)  (pi+||K+) ) ]CC" 
    } 
######################################################################## 
TupleSeq.Members         += [seqB2DD]
TupleSeq.ModeOR           = True
TupleSeq.ShortCircuit     = False


TupleSeq.Members        += [ tuple ] 
TupleSeq.ModeOR          = True 
TupleSeq.ShortCircuit    = False 

TupleSeq.Members        += [ tuple_D0K3pi ] 
TupleSeq.ModeOR          = True 
TupleSeq.ShortCircuit    = False 

TupleSeq.Members         += [ tuple_D02K3piDKKpi ] 
TupleSeq.ModeOR           = True 
TupleSeq.ShortCircuit     = False

######################################################################## 
# 
# DaVinci settings 
# 
DaVinci().EvtMax     = -1
DaVinci().Lumi       = True
DaVinci().InputType  = 'DST'
DaVinci().DataType   = "2016"
CondDB( LatestGlobalTagByDataType = "2016" )
DaVinci().TupleFile       = "DTT.root" # Ntuple

subseq_annpid = GaudiSequencer('SeqANNPID')

from Configurables import ChargedProtoANNPIDConf
conf_ann_pid = ChargedProtoANNPIDConf('ANNPIDConf')
conf_ann_pid.RecoSequencer = subseq_annpid

#DaVinci().UserAlgorithms += [seqB2DstD]
DaVinci().UserAlgorithms += [TupleSeq] 
DaVinci().MainOptions  = ""  

DaVinci().EvtMax     = -1 
DaVinci().PrintFreq  = 5000 
DaVinci().InputType  = "DST" 
DaVinci().DataType   = '2016' 
from Configurables import CondDB 
CondDB( LatestGlobalTagByDataType = "2016")  
#CondDB().UseLatestTags = ['2016'] 
DaVinci().Lumi       = True 
########################################################################  

DaVinci().TupleFile = "B02DstD.root" 
'''
from GaudiConf import IOHelper
IOHelper().inputFiles([
  "/eos/lhcb/user/b/bkhanji/DST/00070442_00003836_1.bhadroncompleteevent.dst"
  ], clear=True)
'''
#importOptions("./2016.py")
