Year = '2011'
########################################################################
from Gaudi.Configuration import *
#######################################################################
#
from PhysSelPython.Wrappers import AutomaticData, Selection, SelectionSequence
from PhysSelPython.Wrappers import (DataOnDemand,
                                    Selection,
                                    SelectionSequence)
import GaudiKernel.SystemOfUnits as Units
from Configurables import (GaudiSequencer,
                           FilterDesktop,
                           CombineParticles,
                           FitDecayTrees
                           )
from Configurables import DaVinci
from GaudiKernel.SystemOfUnits import *
from Configurables import CheckPV
from Configurables import(DecayTreeTuple,
                          TupleToolDecay,
                          TupleToolVtxIsoln,
                          TupleToolTrackIsolation,
                          TupleToolGeneration,
                          TupleToolMuonPid, 
                          TupleToolMCTruth,
                          TupleToolMCBackgroundInfo, 
                          TupleToolRecoStats, 
                          TupleToolMassHypo,
                          TupleToolIsolationTwoBody ,
                          TupleToolAngles, 
                          LoKi__Hybrid__TupleTool,
                          TupleToolTrigger,
                          TupleToolTISTOS ,
                          PrintDecayTree, 
                          PrintDecayTreeTool,
                          TupleToolStripping , 
                          TupleToolTagging,
                          )
from DecayTreeTuple.Configuration import *
########################################################
# DecayTreeTuple

# Filter the tuple :                                                                                                                                             
B02DstD_K3pi_InputLocation = "Phys/B02DstDD02K3PiBeauty2CharmLine/Particles"
B02DstD_Kpi_InputLocation  = "Phys/B02DstDBeauty2CharmLine/Particles"

B0_particles = DataOnDemand( Location ='Phys/B02DstDD02K3PiBeauty2CharmLine/Particles')
B_particles_filter = FilterDesktop("B_particles_filter",
                                   Code =  "(DTF_FUN ( VFASPF(VCHI2/VDOF) , True , strings( ['D*(2010)+', 'D0' , 'D+'] ) ) <1000) & \
                                   (DTF_FUN ( M , True , strings( ['D*(2010)+', 'D0' , 'D+'] ) ) > 0)" )
B_particles_sel    = Selection("B_particles_sel",
                               Algorithm = B_particles_filter,
                               RequiredSelections = [ B0_particles ])

SeqB02DstarDplus = SelectionSequence('SeqB02DstarDplus', TopSelection = B_particles_sel)

from PhysConf.MicroDST import uDstConf
uDstConf ( "/Event/Bhadron" )
from Configurables import DaVinci
DaVinci().RootInTES = '/Event/Bhadron'
from Configurables import TrackScaleState as SCALER
scaler = SCALER( 'Scaler' , RootInTES =  "/Event/Bhadron")
DaVinci().UserAlgorithms = [ scaler ]

TupleSeq   = GaudiSequencer('TupleSeq')

tuple = DecayTreeTuple("D02K3piDKpipi")
tuple.Inputs = [B02DstD_K3pi_InputLocation ]
tuple.Decay = "[ [B0]cc -> ^( D*(2010)- -> ^pi- ^( [D~0]cc -> ^K+ ^pi- ^pi+ ^pi-) ) ^(D+ -> ^K- ^pi+ ^pi+) ]CC"
tuple.ToolList +=  [
    "TupleToolGeometry"
    ,"TupleToolKinematic"
    , "TupleToolTagging"
    , "TupleToolPrimaries"
    , "TupleToolTrackInfo"
    , "TupleToolRecoStats"
    , "TupleToolTISTOS"
    , "TupleToolTrackPosition"
    , "TupleToolGeneration"
    , "LoKi::Hybrid::TupleTool/LoKiTool"
    ]
tuple.ReFitPVs = True
tuple.OutputLevel = 3

tuple.Branches = {
     "slowpi" : "[ [B0]cc ->  ( D*(2010)- ->  ^pi-  ( [D~0]cc ->  (pi-|| K-)  (pi-|| K-)  (pi+ || K+)  (pi+ || K+) ) )  ( D+ ->  (K-|| pi-)  (pi+||K+)  (pi+||K+) ) ]CC"
    ,"pi1_D0" : "[ [B0]cc ->  ( D*(2010)- ->   pi-  ( [D~0]cc -> ^(pi-|| K-)  (pi-|| K-)  (pi+ || K+)  (pi+ || K+) ) )  ( D+ ->  (K-|| pi-)  (pi+||K+)  (pi+||K+) ) ]CC"
    ,"pi2_D0" : "[ [B0]cc ->  ( D*(2010)- ->   pi-  ( [D~0]cc ->  (pi-|| K-) ^(pi-|| K-)  (pi+ || K+)  (pi+ || K+) ) )  ( D+ ->  (K-|| pi-)  (pi+||K+)  (pi+||K+) ) ]CC"
    ,"pi3_D0" : "[ [B0]cc ->  ( D*(2010)- ->   pi-  ( [D~0]cc ->  (pi-|| K-)  (pi-|| K-) ^(pi+ || K+)  (pi+ || K+) ) )  ( D+ ->  (K-|| pi-)  (pi+||K+)  (pi+||K+) ) ]CC"
    ,"K_D0"   : "[ [B0]cc ->  ( D*(2010)- ->   pi-  ( [D~0]cc ->  (pi-|| K-)  (pi-|| K-)  (pi+ || K+) ^(pi+ || K+) ) )  ( D+ ->  (K-|| pi-)  (pi+||K+)  (pi+||K+) ) ]CC"
    ,"D0"     : "[ [B0]cc ->  ( D*(2010)- ->   pi- ^( [D~0]cc ->  (pi-|| K-)  (pi-|| K-)  (pi+ || K+)  (pi+ || K+) ) )  ( D+ ->  (K-|| pi-)  (pi+||K+)  (pi+||K+) ) ]CC"
    ,"Dstar"  : "[ [B0]cc -> ^( D*(2010)- ->   pi-  ( [D~0]cc ->  (pi-|| K-)  (pi-|| K-)  (pi+ || K+)  (pi+ || K+) ) )  ( D+ ->  (K-|| pi-)  (pi+||K+)  (pi+||K+) ) ]CC"
    ,"pi1_Dp" : "[ [B0]cc ->  ( D*(2010)- ->   pi-  ( [D~0]cc ->  (pi-|| K-)  (pi-|| K-)  (pi+ || K+)  (pi+ || K+) ) )  ( D+ ->  (K-|| pi-)  (pi+||K+) ^(pi+||K+) ) ]CC"
    ,"pi2_Dp" : "[ [B0]cc ->  ( D*(2010)- ->   pi-  ( [D~0]cc ->  (pi-|| K-)  (pi-|| K-)  (pi+ || K+)  (pi+ || K+) ) )  ( D+ ->  (K-|| pi-) ^(pi+||K+)  (pi+||K+) ) ]CC"
    ,"K_Dp"   : "[ [B0]cc ->  ( D*(2010)- ->   pi-  ( [D~0]cc ->  (pi-|| K-)  (pi-|| K-)  (pi+ || K+)  (pi+ || K+) ) )  ( D+ -> ^(K-|| pi-)  (pi+||K+)  (pi+||K+) ) ]CC"
    ,"Dp"     : "[ [B0]cc ->  ( D*(2010)- ->   pi-  ( [D~0]cc ->  (pi-|| K-)  (pi-|| K-)  (pi+ || K+)  (pi+ || K+) ) ) ^( D+ ->  (K-|| pi-)  (pi+||K+)  (pi+||K+) ) ]CC"
    ,"B0"     : "[ [B0]cc ->  ( D*(2010)- ->   pi-  ( [D~0]cc ->  (pi-|| K-)  (pi-|| K-)  (pi+ || K+)  (pi+ || K+) ) )  ( D+ ->  (K-|| pi-)  (pi+||K+)  (pi+||K+) ) ]CC"
    }
######################################  
tuple.addTool(TupleToolDecay, name="B0")
TupleToolTagging = TupleToolTagging('TupleToolTagging')
TupleToolTagging.Verbose = True
TupleToolTagging.useFTonDST = True
tuple.addTool(TupleToolTagging)
tuple.ToolList   += [ "TupleToolTagging"]
######################################  
LoKi_B0=LoKi__Hybrid__TupleTool("LoKi_B0")
LoKi_B0.Variables =  {
    "ETA"             : "ETA",
    "PHI"             : "PHI",
    "DTF_TAU"         : "DTF_CTAU( 0, True )/0.299792458",
    "DTF_CTAUS"       : "DTF_CTAUSIGNIFICANCE( 0, True )",
    "DTF_CHI2NDOF"    : "DTF_CHI2NDOF( True )",
    "DTF_MASS_3D"     : "DTF_FUN ( M , True , strings( ['D*(2010)+', 'D0' , 'D+'] ) )" ,
    "DTF_MASS_3DDs"   : "DTF_FUN ( M , True , strings( ['D*(2010)+', 'D0' , 'D_s+'] ) )" ,
    "DTF_VCHI2NDOF_3D": "DTF_FUN ( VFASPF(VCHI2/VDOF) , True , strings( ['D*(2010)+', 'D0' , 'D+'] ) )" ,
    "DTF_VCHI2NDOF_3D_Ds": "DTF_FUN ( VFASPF(VCHI2/VDOF) , True , strings( ['D*(2010)+', 'D0' , 'D_s+'] ) )" ,
    "DTF_MASS_DstD0"  : "DTF_FUN ( M , True , strings( ['D*(2010)+', 'D0'] ))" ,
    "DTF_MASS_DstDp"  : "DTF_FUN ( M , True , strings( ['D*(2010)+', 'D+'] ))" ,
    "DTF_MASS_DstDs"  : "DTF_FUN ( M , True , strings( ['D*(2010)+', 'D_s+'] ))" ,
    "DTF_MASS_D0Dp"   : "DTF_FUN ( M , True , strings( ['D0'      , 'D+'] ))" ,
    "DTF_MASS_D0Ds"   : "DTF_FUN ( M , True , strings( ['D0'      , 'D_s+'] ))" ,
    "DTF_MASS_D0"     : "DTF_FUN ( M , True , 'D0' )" ,
    "DTF_MASS_Dp"     : "DTF_FUN ( M , True , 'D+' )" ,
    "DTF_MASS_Dst"    : "DTF_FUN ( M , True , 'D*(2010)+' )" ,
    "DTF_VCHI2NDOF"   : "DTF_FUN ( VFASPF(VCHI2/VDOF) , True  )",
    "DTF_VtxZ"        : "DTF_FUN ( BPVVDR , True ,strings( ['D*(2010)+', 'D0' , 'D+'] ) )",
    "Rho_Dist"        : "BPVVDR",
    "CORRM"           : "BPVCORRM",
    "Costheta_star1"  : "LV01",
    "DOCA"            : "DOCA(1,2)",
    "DTF_CTAU"        : "DTF_CTAU( 0, True )",
    "FDS"             : "BPVDLS" ,
    "ptasy_1.50"      : "RELINFO('/Event/Bhadron/Phys/B02DstDD02K3PiBeauty2CharmLine/P2ConeVar1','CONEPTASYM',-1000.)",
    "COSPOL_D0"       : "COSPOL( '[[B0]cc -> (D*(2010)- ->  pi- ^([D~0]cc -> K+ pi- pi+ pi-)) (D+ -> K- pi+ pi+)]CC','[Beauty -> ^(D*(2010)- -> pi- ([D~0]cc -> K+ pi- pi+ pi-)) (D+ -> K- pi+ pi+)]CC' )",
    "COSPOL_pi1_D0"   : "COSPOL( '[[B0]cc -> (D*(2010)- -> pi- ([D~0]cc -> K+ ^pi- pi+ pi-)) (D+ -> K- pi+ pi+)]CC','[Beauty -> (D*(2010)- -> pi- ^([D~0]cc -> K+ pi- pi+ pi-)) (D+ -> K- pi+ pi+)]CC' ) ",
    "COSPOL_pi2_D0"   : "COSPOL( '[[B0]cc -> (D*(2010)- ->  pi-  ([D~0]cc -> K+ pi- ^pi+ pi-)) (D+ ->  K-  pi+  pi+)]CC' ,'[Beauty ->  (D*(2010)- -> pi- ^([D~0]cc -> K+ pi- pi+ pi-))  (D+ -> K- pi+ pi+)]CC' ) ",
    "COSPOL_pi3_D0"   : "COSPOL( '[[B0]cc -> (D*(2010)- ->  pi-  ([D~0]cc -> K+ pi- pi+ ^pi-)) (D+ ->  K-  pi+  pi+)]CC' ,'[Beauty ->  (D*(2010)- -> pi- ^([D~0]cc -> K+ pi- pi+ pi-))  (D+ -> K- pi+ pi+)]CC' ) ",
    "COSPOL_pi_Dst"   : "COSPOL( '[[B0]cc -> (D*(2010)- -> ^pi-  ([D~0]cc -> K+ pi- pi+ pi-)) (D+ ->  K-  pi+  pi+)]CC' ,'[Beauty -> ^(D*(2010)- -> pi-  ([D~0]cc -> K+ pi- pi+ pi-))  (D+ -> K- pi+ pi+)]CC' )",
    "COSPOL_K_Dp"     : "COSPOL( '[[B0]cc -> (D*(2010)- ->  pi- ([D~0]cc -> K+ pi- pi+ pi-)) (D+ -> ^K-  pi+  pi+)]CC' ,'[Beauty ->  (D*(2010)- -> pi-  ([D~0]cc -> K+ pi- pi+ pi-)) ^(D+ -> K- pi+ pi+)]CC' ) ",
    "COSPOL_pim_Dp"   : "COSPOL( '[[B0]cc -> (D*(2010)- ->  pi-  ([D~0]cc -> K+ pi- pi+ pi-)) (D+ ->  K-  pi+ ^pi+)]CC' ,'[Beauty ->  (D*(2010)- -> pi-  ([D~0]cc -> K+ pi- pi+ pi-)) ^(D+ -> K- pi+ pi+)]CC' ) ",
    "COSPOL_pip_Dp"   : "COSPOL( '[[B0]cc -> (D*(2010)- ->  pi-  ([D~0]cc -> K+ pi- pi+ pi-)) (D+ ->  K- ^pi+  pi+)]CC' ,'[Beauty ->  (D*(2010)- -> pi- ([D~0]cc -> K+ pi- pi+ pi-)) ^(D+ -> K- pi+ pi+)]CC' )"
}    
tuple.B0.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_B0"]
tuple.B0.addTool(LoKi_B0)
######################################
tuple.addTool(TupleToolDecay, name="Dstar")
tuple.addTool(TupleToolDecay, name="D0")
tuple.addTool(TupleToolDecay, name="Dp")

LoKi_Dstar=LoKi__Hybrid__TupleTool("LoKi_Dstar")
LoKi_Dstar.Variables =  {
    "LOKI_MASS_D0"        : "DTF_FUN ( M , True , 'D0' )" ,
    "DTF_CTAU"            : "DTF_CTAU( 0, True )",
    "LOKI_DTF_VCHI2NDOF"  : "DTF_FUN ( VFASPF(VCHI2/VDOF) , True  )",
    "Dist_DTF_z"          : "DTF_FUN ( BPVVDR , True , 'D0')",
    "Dist_z"              : "BPVVDR",
    "CORRM"               : "BPVCORRM",
    "Costheta_star1"      : "LV01",
    "Costheta_star2"      : "LV02",
    "DOCA"                : "DOCA(1,2)"
    }
tuple.Dstar.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_Dstar"]
tuple.Dstar.addTool(LoKi_Dstar)

LoKi_D0=LoKi__Hybrid__TupleTool("LoKi_D0")
LoKi_D0.Variables =  {
    "DTF_CTAUS"       : "DTF_CTAUSIGNIFICANCE( 0, True )",
    "DTF_CTAU"        : "DTF_CTAU( 0, True )",
    "DTF_CHI2NDOF"    : "DTF_CHI2NDOF( True )",
    "FDS"             : "BPVDLS",
    "Dist_z"          : "BPVVDR",
    "Costheta_star0"  : "LV01",
    "CORRM"           : "BPVCORRM",
    "Costheta_star2"  : "LV02",
    "DOCA"            : "DOCA(1,2)",
    "M12"             : "MASS(1,2)",
    "M14"             : "MASS(1,4)",
    "M23"             : "MASS(2,3)",
    "M34"             : "MASS(3,4)"
    }
tuple.D0.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_D0"]
tuple.D0.addTool(LoKi_D0)

LoKi_Dp=LoKi__Hybrid__TupleTool("LoKi_Dp")
LoKi_Dp.Variables =  {
    "DTF_CTAUS"       : "DTF_CTAUSIGNIFICANCE( 0, True )",
    "DTF_CTAU"        : "DTF_CTAU( 0, True )",
    "DTF_CHI2NDOF"    : "DTF_CHI2NDOF( True )",
    "FDS"             : "BPVDLS",
    "Inv_mass_12"     : "M12",
    "Inv_mass_13"     : "M13",
    "Inv_mass_23"     : "M23",
    "Dist_z"          : "BPVVDR",
    "Dist_DTF_z"      : "DTF_FUN(BPVVDR ,True , 'D0')",
    "Lc_mass1"        : "WM('K-' ,'p+'  , 'pi+')" ,
    "Lc_mass2"        : "WM('K-' ,'pi+' , 'p+')" ,
    "Ds_mass1"        : "WM('K-' ,'K+'  , 'pi+')" ,
    "Ds_mass2"        : "WM('K-' ,'pi+'  , 'K+')" ,
    "phi_mass"        : "WM('K-' ,'K+')",
    "Costheta_star0"  : "LV01",
    "Costheta_star2"  : "LV02",
    "Costheta_star3"  : "LV03",
    "DOCA1"           : "DOCA(1,2)",
    "DOCA2"           : "DOCA(3,2)",
    "DOCA3"           : "DOCA(1,3)",
    "CORRM"           : "BPVCORRM"
    }
tuple.Dp.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_Dp"]
tuple.Dp.addTool(LoKi_Dp)
######################################
TupleToolTISTOS = TupleToolTISTOS('TupleToolTISTOS')
TupleToolTISTOS.TriggerList=[
    "Hlt2Topo2BodyBBDTDecision",
    "Hlt2Topo3BodyBBDTDecision",
    "Hlt2Topo4BodyBBDTDecision",
    "Hlt2CharmHadD02HHHHDecision",
    "Hlt2IncPhiDecision",
    "Hlt2CharmHadD02HHHHWideMassDecision"
    #"Hlt2TopoMu2BodyBBDTDecision",
    #"Hlt2TopoMu3BodyBBDTDecision",
    #"Hlt2TopoMu4BodyBBDTDecision",
    #"Hlt2TopoMu2BodySimpleDecision",
    #"Hlt2TopoMu3BodySimpleDecision",
    #"Hlt2TopoMu4BodySimpleDecision",
    #"Hlt2Topo2BodySimpleDecision",
    #"Hlt2Topo3BodySimpleDecision",
    #"Hlt2Topo4BodySimpleDecision",
]
TupleToolTISTOS.VerboseL0 = True
TupleToolTISTOS.VerboseHlt1 = True
TupleToolTISTOS.VerboseHlt2 = True

tuple.B0.addTool(TupleToolTISTOS , name = 'TupleToolTISTOS' )
tuple.B0.ToolList   += [ "TupleToolTISTOS"]

######################################################################
TupleSeq.Members        += [ tuple ]
TupleSeq.ModeOR          = True
TupleSeq.ShortCircuit    = False
########################################################################
tuple_D02K3piDKKpi        =  tuple.clone("D02K3piDKKpi")
tuple_D02K3piDKKpi.Decay  = "[ [B0]cc -> ^( D*(2010)- -> ^pi- ^( [D~0]cc -> ^K+ ^pi- ^pi+ ^pi-) ) ^(D+ -> ^K- ^K+ ^pi+) ]CC"
tuple_D02K3piDKKpi.Inputs =  [B02DstD_K3pi_InputLocation]
tuple_D02K3piDKKpi.Branches = {
     "slowpi" : "[ [B0]cc ->  ( D*(2010)- ->  ^pi-  ( [D~0]cc ->  (pi-|| K-)  (pi-|| K-)  (pi+ || K+)  (pi+ || K+) ) )  ( D+ ->  (K-|| pi-)  (pi+||K+)  (pi+||K+) ) ]CC"
    ,"pi1_D0" : "[ [B0]cc ->  ( D*(2010)- ->   pi-  ( [D~0]cc -> ^(pi-|| K-)  (pi-|| K-)  (pi+ || K+)  (pi+ || K+) ) )  ( D+ ->  (K-|| pi-)  (pi+||K+)  (pi+||K+) ) ]CC"
    ,"pi2_D0" : "[ [B0]cc ->  ( D*(2010)- ->   pi-  ( [D~0]cc ->  (pi-|| K-) ^(pi-|| K-)  (pi+ || K+)  (pi+ || K+) ) )  ( D+ ->  (K-|| pi-)  (pi+||K+)  (pi+||K+) ) ]CC"
    ,"pi3_D0" : "[ [B0]cc ->  ( D*(2010)- ->   pi-  ( [D~0]cc ->  (pi-|| K-)  (pi-|| K-) ^(pi+ || K+)  (pi+ || K+) ) )  ( D+ ->  (K-|| pi-)  (pi+||K+)  (pi+||K+) ) ]CC"
    ,"K_D0"   : "[ [B0]cc ->  ( D*(2010)- ->   pi-  ( [D~0]cc ->  (pi-|| K-)  (pi-|| K-)  (pi+ || K+) ^(pi+ || K+) ) )  ( D+ ->  (K-|| pi-)  (pi+||K+)  (pi+||K+) ) ]CC"
    ,"D0"     : "[ [B0]cc ->  ( D*(2010)- ->   pi- ^( [D~0]cc ->  (pi-|| K-)  (pi-|| K-)  (pi+ || K+)  (pi+ || K+) ) )  ( D+ ->  (K-|| pi-)  (pi+||K+)  (pi+||K+) ) ]CC"
    ,"Dstar"  : "[ [B0]cc -> ^( D*(2010)- ->   pi-  ( [D~0]cc ->  (pi-|| K-)  (pi-|| K-)  (pi+ || K+)  (pi+ || K+) ) )  ( D+ ->  (K-|| pi-)  (pi+||K+)  (pi+||K+) ) ]CC"
    ,"pi1_Dp" : "[ [B0]cc ->  ( D*(2010)- ->   pi-  ( [D~0]cc ->  (pi-|| K-)  (pi-|| K-)  (pi+ || K+)  (pi+ || K+) ) )  ( D+ ->  (K-|| pi-)  (pi+||K+) ^(pi+||K+) ) ]CC"
    ,"pi2_Dp" : "[ [B0]cc ->  ( D*(2010)- ->   pi-  ( [D~0]cc ->  (pi-|| K-)  (pi-|| K-)  (pi+ || K+)  (pi+ || K+) ) )  ( D+ ->  (K-|| pi-) ^(pi+||K+)  (pi+||K+) ) ]CC"
    ,"K_Dp"   : "[ [B0]cc ->  ( D*(2010)- ->   pi-  ( [D~0]cc ->  (pi-|| K-)  (pi-|| K-)  (pi+ || K+)  (pi+ || K+) ) )  ( D+ -> ^(K-|| pi-)  (pi+||K+)  (pi+||K+) ) ]CC"
    ,"Dp"     : "[ [B0]cc ->  ( D*(2010)- ->   pi-  ( [D~0]cc ->  (pi-|| K-)  (pi-|| K-)  (pi+ || K+)  (pi+ || K+) ) ) ^( D+ ->  (K-|| pi-)  (pi+||K+)  (pi+||K+) ) ]CC"
    ,"B0"     : "[ [B0]cc ->  ( D*(2010)- ->   pi-  ( [D~0]cc ->  (pi-|| K-)  (pi-|| K-)  (pi+ || K+)  (pi+ || K+) ) )  ( D+ ->  (K-|| pi-)  (pi+||K+)  (pi+||K+) ) ]CC"
    }
TupleSeq.Members         += [ tuple_D02K3piDKKpi ]
TupleSeq.ModeOR           = True
TupleSeq.ShortCircuit     = False
########################################################################
#                                                                                                                                                                                                           
# Event Tuple                                                                                                                                                                                               
#                                                                                                                                                                                                           
from Configurables import EventTuple , TupleToolEventInfo
etuple = EventTuple()
etuple.ToolList += ["TupleToolEventInfo"]
########################################################################
#
# DaVinci settings
#
DaVinci().UserAlgorithms += [TupleSeq]
DaVinci().MainOptions  = "" 
DaVinci().EvtMax       = -1
DaVinci().PrintFreq   = 50000
DaVinci().InputType = "MDST"
DaVinci().DataType     = Year
from Configurables import CondDB
CondDB().UseLatestTags = [Year]
DaVinci().Lumi = True
######################################################################## 

#DaVinci().appendToMainSequence( [ sc.sequence() ] )
# data:
#from Configurables import CondDB 
#CondDB(UseOracle = True) 
#data 2011:
#DaVinci().DDDBtag = 'head-20110914'
#DaVinci().CondDBtag = 'head-20111111' #'sim-20100831-vc-md100'
# data 2012:
#DaVinci().DDDBtag       =  "dddb-20120831" # MC12
#DaVinci().CondDBtag     =  "sim-20141210-1-vc-mu100" # MC12
#CondDB().UseLatestTags = ["2012"]
#DaVinci().Lumi=True
# MC
#DaVinci().Simulation = True
#DaVinci().DDDBtag       =  "dddb-20130929-1" #"dddb-20150526" # MC12_u
#DaVinci().DDDBtag       = "dddb-20130929-1"  #"dddb-20150526" # MC12_d

#DaVinci().CondDBtag     =  "sim-20130522-1-vc-mu100" #"sim-20150520-vc-mu100" # MC12_u
#DaVinci().CondDBtag     = "sim-20130522-1-vc-md100"  #"sim-20150520-vc-md100" # MC12_d
#DaVinci().Lumi = False        
#######################################################################
#
#DaVinci().TupleFile = "DTT_MCB02DstD_K3pi_2012Down.root"
#importOptions("/afs/cern.ch/user/n/nbelloli/DstD/MCDstD.py") #

#from GaudiConf import IOHelper
# Use the local input data
#IOHelper().inputFiles([
#    '/afs/cern.ch/user/b/bkhanji/private/DVana/B2DstarD/00049476_00000084_2.AllStreams.dst'
#        ], clear=True)
DaVinci().TupleFile = "B02DstD_K3pi_2011Up.root"

#importOptions("DATADstD.py")#
