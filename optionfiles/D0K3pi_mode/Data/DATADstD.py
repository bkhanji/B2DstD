#-- GAUDI jobOptions generated on Tue May 17 11:29:34 2016
#-- Contains event types : 
#--   90000000 - 5 files - 7362 events - 0.08 GBytes


#--  Extra information about the data processing phases:


#--  Processing Pass Step-129288 

#--  StepId : 129288 
#--  StepName : Stripping21r0p1-Merging-DV-v39r1p1-AppConfig-v3r257 
#--  ApplicationName : DaVinci 
#--  ApplicationVersion : v39r1p1 
#--  OptionFiles : $APPCONFIGOPTS/Merging/DV-Stripping-Merging.py 
#--  DDDB : dddb-20150928 
#--  CONDDB : cond-20150409-1 
#--  ExtraPackages : AppConfig.v3r257;SQLDDDB.v7r10 
#--  Visible : N 

from Gaudi.Configuration import * 
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles(['LFN:/lhcb/LHCb/Collision12/BHADRON.MDST/00050929/0000/00050929_00000001_1.bhadron.mdst',
'LFN:/lhcb/LHCb/Collision12/BHADRON.MDST/00050929/0000/00050929_00000011_1.bhadron.mdst',
'LFN:/lhcb/LHCb/Collision12/BHADRON.MDST/00050929/0000/00050929_00000024_1.bhadron.mdst',
'LFN:/lhcb/LHCb/Collision12/BHADRON.MDST/00050929/0000/00050929_00000039_1.bhadron.mdst',
'LFN:/lhcb/LHCb/Collision12/BHADRON.MDST/00050929/0000/00050929_00000059_1.bhadron.mdst'
], clear=True)
FileCatalog().Catalogs += [ 'xmlcatalog_file:DATADstD.xml' ]
