"""
 Script to create a DecayTreeTuple for B2DD analysis:
"""
__author__ = "Frank Meier <frank.meier@tu-dortmund.de>"

##############################################################################################
##### SETTINGS
##############################################################################################
import GaudiKernel.SystemOfUnits as Units
from Gaudi.Configuration import *
from DecayTreeTuple.Configuration import *
from Configurables import (BDecayTool,
                           BTagging,
                           BTaggingTool,
                           CheckPV,
                           CombineParticles,
                           DecayTreeTuple,
                           EventTuple,
                           FilterDesktop,
                           FitDecayTrees,
                           GaudiSequencer,
                           L0TriggerTisTos,
                           LoKi__Hybrid__TupleTool,
                           OfflineVertexFitter,
                           PrintDecayTree,
                           PrintDecayTreeTool,
                           TaggingUtilsChecker,
                           TESCheck,
                           TriggerTisTos,
                           TupleToolAngles,
                           TupleToolDecay,
                           TupleToolDecayTreeFitterExtended,
                           TupleToolRecoStats,
                           TupleToolTagging,
                           TupleToolTISTOS,
                           TupleToolStripping,
                           TupleToolTrackPosition,
                           TupleToolMCTruth,
                           TupleToolMCBackgroundInfo,
                           TupleToolTrigger,
                           TupleToolEventInfo,
                           TupleToolVtxIsoln,
                           MCTupleToolHierarchy,
                           TupleToolGeometry,
                           TupleToolTrackInfo,
                           TupleToolPid,
                           TupleToolANNPID,
                           TupleToolMassHypo)
import re
from PhysSelPython.Wrappers import Selection, SelectionSequence
from StandardParticles import StdLoosePions, StdAllNoPIDsPions
from Configurables import TrackScaleState
from Configurables import TrackSmearState as SMEAR
from Configurables import CondDB
from Configurables import DaVinci

'''
DaVinci().RootInTES = '/Event/Bhadron'
from Configurables import TrackScaleState 
scaler = TrackScaleState('scaler')
DaVinci().UserAlgorithms += [scaler]
'''
prefix = 'AllStreams/'
##############################################################################################
##### IMPORTS
##############################################################################################
from PhysSelPython.Wrappers import Selection, SelectionSequence
from StandardParticles import StdLoosePions, StdAllNoPIDsPions
##############################################################################################
##### PRESELECTION
##############################################################################################
from Configurables import LoKi__HDRFilter as StripFilter

from PhysSelPython.Wrappers import DataOnDemand
from PhysConf.Selections import AutomaticData, MomentumScaling

#inputDataOnDemandB2DD = DataOnDemand(Location = 'Phys/B02DDBeauty2CharmLine/Particles')
inputDataOnDemandB2DD = AutomaticData(Location =  prefix + 'Phys/B02DDBeauty2CharmLine/Particles')
_tighterDs = FilterDesktop("TighterDs")

D1MassWindow = "( CHILDCUT( ADMASS('D+')<50*MeV, 1)) "
D2MassWindow = "( CHILDCUT( ADMASS('D+')<50*MeV, 2)) "
DMassWindowL = "(" + D1MassWindow + " & " + D2MassWindow + ")"


D1MassWindowA = "( CHILDCUT( ADMASS('D_s+')<50*MeV, 1)) "
D2MassWindowA = "( CHILDCUT( ADMASS('D+')<50*MeV, 2)) "
DMassWindowA = "(" + D1MassWindowA + " & " + D2MassWindowA + ")"


D1MassWindowB = "( CHILDCUT( ADMASS('D_s+')<50*MeV, 2)) "
D2MassWindowB = "( CHILDCUT( ADMASS('D+')<50*MeV, 1)) "
DMassWindowB = "(" + D1MassWindowB + " & " + D2MassWindowB + ")"

DMassWindow = "(" + DMassWindowL + "|" + DMassWindowB + "|" + DMassWindowA + ")"

_tighterDs.Code = DMassWindow
SelTighterDs = Selection("SelTighterDs", Algorithm = _tighterDs, RequiredSelections = [inputDataOnDemandB2DD])
SelSeqTighterDs = SelectionSequence("SelSeqTighterDs", TopSelection = SelTighterDs)
seqDs = SelSeqTighterDs.sequence()

##############################################################################################
##### Upgrade of _TRACK_GhostProb for downstream tracks
##############################################################################################
from STTools import STOfflineConf
STOfflineConf.DefaultConf().configureTools()
from Configurables import RefitParticleTracks
refitter = RefitParticleTracks()
refitter.DoFit = True
# shouldn't make a difference. either confirm that, or leave it as it is
refitter.DoProbability = True
refitter.UpdateProbability = True
refitter.ReplaceTracks = True
# shouldn't make a difference. either confirm that, or leave it as it is

## I never fully convinced myself whether the following three lines are
## necessary or not. Either way they're not wrong:
from Configurables import TrackInitFit, TrackMasterFitter
from TrackFitter.ConfiguredFitters import ConfiguredMasterFitter

refitter.addTool(TrackInitFit,"TrackInitFit")
refitter.TrackInitFit.addTool(TrackMasterFitter,"Fit")
ConfiguredMasterFitter( refitter.TrackInitFit.Fit )

B2DDtuple = DecayTreeTuple("B02DD")

B2DDtuple.Inputs = [SelSeqTighterDs.outputLocation()]
# dominant final state for DD
#B2DDtuple.Decay = "[B0 -> ^(D+ -> ^K- ^pi+ ^pi+) ^(D- -> ^K+ ^pi- ^pi-)]CC"
# dominant DsD final state and subdominant DD final state
B2DDtuple.Decay  = "[(B0 -> ^(D+ -> ^(K-) ^(pi+) ^(pi+)) ^(D- -> ^(K+) ^(K-) ^(pi-))) || "
B2DDtuple.Decay  += "(B0 -> ^(D+ -> ^(K-) ^(K+) ^(pi+)) ^(D- -> ^(K+) ^(pi-) ^(pi-)))]CC"
# third DD final state
#B2DDtuple.Decay  = "[B0 -> ^(D+ -> ^(K-) ^(K+) ^(pi+)) ^(D- -> ^(K+) ^(K-) ^(pi-))]CC"
# any final state
#B2DDtuple.Decay = "[B0 -> ^(D+ -> ^(K- || pi-) ^(pi+ || K+) ^(pi+ || K+)) ^(D- -> ^(K+ || pi+) ^(pi- || K-) ^(pi- || K-))]CC"
B2DDtuple.ReFitPVs = False
##############################################################################################
##### STANDARD TOOLS AND STRIPPING LINE
##############################################################################################
tuple_tools = ["TupleToolKinematic",
               "TupleToolPropertime",
               "TupleToolRecoStats" ,
               "TupleToolPrimaries" ,
               "TupleToolMCBackgroundInfo"
               ]

B2DDtuple.ToolList = tuple_tools

tt_pid = B2DDtuple.addTupleTool("TupleToolPid")
tt_pid.Verbose = True

tt_geometry = B2DDtuple.addTupleTool("TupleToolGeometry")
tt_geometry.Verbose = False
tt_geometry.RefitPVs = False
tt_geometry.FillMultiPV = False

tt_trackinfo = B2DDtuple.addTupleTool("TupleToolTrackInfo") # matchChi2 etc.
tt_trackinfo.Verbose = True

tt_eventinfo = B2DDtuple.addTupleTool("TupleToolEventInfo")

branches = {}
descriptor_B = "([B0 -> (D+ -> (K- || pi-) (pi+ || K+) (pi+ || K+)) (D- -> (K+ || pi+) (pi- || K-) (pi- || K-))]CC)"
branches["B0"] = "^(" + descriptor_B + ")"

add_branches = { "Dplus"  : ( "D+", { #"substitute"                  : [ {"K-" : "K-"}, {"K-" : "pi-"}, {"pi+" : "K+"} ],
                                      #"LOKI_DOCA"                   : "ACUTDOCA",
                                      "LOKI_VertexSeparation_CHI2"  : "BPVVDCHI2" }),
                 "Dminus" : ( "D-", { #"substitute"                  : [ {"K+" : "K+"}, {"K+" : "pi+"}, {"pi-" : "K-"} ],
                                      #"LOKI_DOCA"                   : "ACUTDOCA",
                                      "LOKI_VertexSeparation_CHI2"  : "BPVVDCHI2" })}

for name_branch, value in add_branches.iteritems():
  branches[name_branch] = descriptor_B.replace("(" + value[0], "^(" + value[0])
  B2DDtuple.addTupleTool(TupleToolDecay, name=name_branch)
  branch_infos = value[1]
  if "substitute" in branch_infos:
    substitutions = branch_infos.pop("substitute")
    i = 0
    for substitution in substitutions:
      tt_masshypo = B2DDtuple.__getattr__(name_branch).addTupleTool("TupleToolMassHypo/TTMassHypo_" + str(i))
      tt_masshypo.PIDReplacements = substitution
      i += 1
  if len(branch_infos) > 0:
    tt_loki_br = B2DDtuple.__getattr__(name_branch).addTupleTool("LoKi::Hybrid::TupleTool/tt_loki_"+name_branch)
    tt_loki_br.Variables = branch_infos
  else:
    print "For branch " + name_branch + " no other LOKI variables specified."

branches["Dplus_Kminus_or_piminus"] = "[B0 -> (D+ -> ^(K- || pi-) (pi+ || K+) (pi+ || K+)) (D- -> (K+ || pi+) (pi- || K-) (pi- || K-))]CC"
branches["Dplus_piplus_or_Kplus_One"] = "[B0 -> (D+ -> (K- || pi-) ^(pi+ || K+) (pi+ || K+)) (D- -> (K+ || pi+) (pi- || K-) (pi- || K-))]CC"
branches["Dplus_piplus_or_Kplus_Two"] = "[B0 -> (D+ -> (K- || pi-) (pi+ || K+) ^(pi+ || K+)) (D- -> (K+ || pi+) (pi- || K-) (pi- || K-))]CC"
branches["Dminus_Kplus_or_piplus"] = "[B0 -> (D+ -> (K- || pi-) (pi+ || K+) (pi+ || K+)) (D- -> ^(K+ || pi+) (pi- || K-) (pi- || K-))]CC"
branches["Dminus_piminus_or_Kminus_One"] = "[B0 -> (D+ -> (K- || pi-) (pi+ || K+) (pi+ || K+)) (D- -> (K+ || pi+) ^(pi- || K-) (pi- || K-))]CC"
branches["Dminus_piminus_or_Kminus_Two"] = "[B0 -> (D+ -> (K- || pi-) (pi+ || K+) (pi+ || K+)) (D- -> (K+ || pi+) (pi- || K-) ^(pi- || K-))]CC"

B2DDtuple.addBranches(branches)

B2DDtuple.addTupleTool(TupleToolDecay, name="B0")

##############################################################################################
##### TAGGING (not available before Stripping 21)
##############################################################################################
from Configurables import (BTaggingTool,
                           OSMuonTagger,
                           OSElectronTagger,
                           OSKaonTagger,
                           OSVtxChTagger)
OptimizationVersion = 'Summer2017OptimisationDev'

from FlavourTagging.Tunings import applyTuning as applyFTTuning
tt_tagging1         = B2DDtuple.addTupleTool("TupleToolTagging")
tt_tagging1.UseFTfromDST = True
tt_tagging1.Verbose = True
tt_tagging1.AddMVAFeatureInfo = False
tt_tagging1.AddTagPartsInfo   = False
tt_tagging1.OutputLevel       = INFO

from Configurables import MCMatchObjP2MCRelator
MCTruth= B2DDtuple.addTupleTool("TupleToolMCTruth")
default_rel_locs = MCMatchObjP2MCRelator().getDefaultProperty('RelTableLocations')
rel_locs = [loc for loc in default_rel_locs if 'Turbo' not in loc]
MCTruth.addTool(MCMatchObjP2MCRelator)
MCTruth.MCMatchObjP2MCRelator.RelTableLocations = rel_locs
MCTruth.addTupleTool("MCTupleToolHierarchy")
##############################################################################################
##### TRIGGER
##############################################################################################

hlt1_lines = ['L0PhysicsDecision',
              'L0MuonDecision',
              'L0DiMuonDecision',
              'L0MuonHighDecision',
              'L0HadronDecision',
              'L0ElectronDecision',
              'L0PhotonDecision',
              'Hlt1TrackAllL0Decision',
              'Hlt1TrackAllL0TightDecision',
              'Hlt1L0AnyDecision',
              'Hlt1GlobalDecision']

hlt2_lines = ['Hlt2GlobalDecision'
              ,"Hlt1TrackAllL0Decision"
              ,"Hlt1TrackMVADecision"
              ,"Hlt1TwoTrackMVADecision"
              ,"Hlt2Topo2BodyDecision"
              ,"Hlt2Topo3BodyDecision"
              ,"Hlt2Topo4BodyDecision"
              ,'Hlt2Topo2BodyBBDTDecision'
              ,'Hlt2Topo3BodyBBDTDecision'
              ,'Hlt2Topo4BodyBBDTDecision'
              # 'Hlt2TopoMu2BodyBBDTDecision',
              # 'Hlt2TopoMu3BodyBBDTDecision',
              # 'Hlt2TopoMu4BodyBBDTDecision',
              # 'Hlt2TopoE2BodyBBDTDecision',
              # 'Hlt2TopoE3BodyBBDTDecision',
              # 'Hlt2TopoE4BodyBBDTDecision',
              ,'Hlt2IncPhiDecision'
              ,'Hlt2IncPhiSidebandsDecision']

trigger_lines = hlt1_lines + hlt2_lines
tt_trigger = B2DDtuple.addTupleTool("TupleToolTrigger")
tt_trigger.Verbose = True
tt_trigger.TriggerList = trigger_lines

tt_tistos = B2DDtuple.__getattr__("B0").addTupleTool("TupleToolTISTOS")
tt_tistos.Verbose = True
tt_tistos.TriggerList = trigger_lines

##############################################################################################
##### LOKI VARIABLES
##############################################################################################

loki_variables = {"LOKI_ENERGY"     : "E",
                  "LOKI_ETA"        : "ETA",
                  "LOKI_PHI"        : "PHI"}

tt_loki_general = B2DDtuple.addTupleTool("LoKi::Hybrid::TupleTool/tt_loki_general")
tt_loki_general.Variables = loki_variables

loki_variables_B = {"LOKI_FDCHI2"                 : "BPVVDCHI2",
                    "LOKI_FDS"                    : "BPVDLS",
                    "LOKI_DIRA"                   : "BPVDIRA",
                    ###### DecayTreeFitVariables
                    "LOKI_DTF_CTAU"               : "DTF_CTAU( 0, True )",
                    "LOKI_DTF_CTAU_NOPV"          : "DTF_CTAU( 0, False )",
                    "LOKI_DTF_CTAUS"              : "DTF_CTAUSIGNIFICANCE( 0, True )",
                    "LOKI_DTF_CHI2NDOF"           : "DTF_CHI2NDOF( True )",
                    "LOKI_DTF_CTAUERR"            : "DTF_CTAUERR( 0, True )",
                    "LOKI_DTF_CTAUERR_NOPV"       : "DTF_CTAUERR( 0, False )",
                    "LOKI_MASS_DplusConstr"       : "DTF_FUN ( M , True , 'D+' )" ,
                    "LOKI_MASS_Dplus_NoPVConstr"  : "DTF_FUN ( M , False , 'D+' )" ,
                    "LOKI_MASS_DminusConstr"      : "DTF_FUN ( M , True , 'D-' )" ,
                    "LOKI_MASS_Dminus_NoPVConstr" : "DTF_FUN ( M , False , 'D-' )" ,
                    "LOKI_MASSERR_DplusConstr"    : "sqrt(DTF_FUN ( M2ERR2 , True , 'D+' ))" ,
                    "LOKI_MASSERR_DminusConstr"   : "sqrt(DTF_FUN ( M2ERR2 , True , 'D-' ))" ,
                    "LOKI_DTF_VCHI2NDOF"          : "DTF_FUN ( VFASPF(VCHI2/VDOF) , True )",
                    "LOKI_DTF_CTAU_D1"            : "DTF_CTAU(1, True)",
                    "LOKI_DTF_CTAU_D2"            : "DTF_CTAU(2, True)",
                    "LOKI_DTF_CTAUERR_D1"         : "DTF_CTAUERR(1, True)",
                    "LOKI_DTF_CTAUERR_D2"         : "DTF_CTAUERR(2, True)",
                    "LOKI_MASS_DDConstr"          : "DTF_FUN ( M , True , strings ( ['D+','D-'] ) )" ,
                    "LOKI_MASSERR_DDConstr"       : "sqrt(DTF_FUN ( M2ERR2 , True , strings(['D+','D-'])))"}

tt_loki_B = B2DDtuple.__getattr__("B0").addTupleTool("LoKi::Hybrid::TupleTool/tt_loki_B")
tt_loki_B.Variables = loki_variables_B

##############################################################################################
##### CONSTRAINTS
##############################################################################################

#No constraints at all
Fit = B2DDtuple.__getattr__("B0").addTupleTool("TupleToolDecayTreeFitterExtended/Fit")
Fit.Verbose = True
#Fit.OutputLevel = VERBOSE
Fit.UpdateDaughters = True
Fit.constrainToOriginVertex = False

#Constraint only on PV 
FitPVConst = B2DDtuple.__getattr__("B0").addTupleTool("TupleToolDecayTreeFitterExtended/FitPVConst")
FitPVConst.Verbose = True
#FitPVConst.OutputLevel = VERBOSE
FitPVConst.UpdateDaughters = True
FitPVConst.constrainToOriginVertex = True

#Constraint on daughters but not on PV
FitDaughtersConst = B2DDtuple.__getattr__("B0").addTupleTool("TupleToolDecayTreeFitterExtended/FitDDConst")
FitDaughtersConst.Verbose = True
#FitDaughtersConst.OutputLevel = VERBOSE
FitDaughtersConst.UpdateDaughters = True
FitDaughtersConst.daughtersToConstrain = ["D+","D-"]
FitDaughtersConst.constrainToOriginVertex = False

#Constraint on daughters and constraint to OriginPV
FitDaughtersPVConst = B2DDtuple.__getattr__("B0").addTupleTool("TupleToolDecayTreeFitterExtended/FitDDPVConst")
FitDaughtersPVConst.Verbose = True
#FitDaughtersPVConst.OutputLevel = VERBOSE
FitDaughtersPVConst.UpdateDaughters = True
FitDaughtersPVConst.daughtersToConstrain = ["D+","D-"]
FitDaughtersPVConst.constrainToOriginVertex = True

FitDaughters_strange1Const = B2DDtuple.__getattr__("B0").addTupleTool("TupleToolDecayTreeFitterExtended/FitDsDConst")
#FitDaughters_strange1Const.Verbose = True
#FitDaughters_strange1PVConst.OutputLevel = VERBOSE
#FitDaughters_strange1Const.UpdateDaughters = True
FitDaughters_strange1Const.UpdateDaughters = False
FitDaughters_strange1Const.daughtersToConstrain = ["D_s+","D-"]
FitDaughters_strange1Const.constrainToOriginVertex = False
FitDaughters_strange1Const.Substitutions = { "B0 -> ^(D+ -> K- K+ pi+) (D- -> K+ pi- pi-)" : "D_s+"}


##Constraint on daughters and constraint to OriginPV
FitDaughters_strange2Const = B2DDtuple.__getattr__("B0").addTupleTool("TupleToolDecayTreeFitterExtended/FitDDsConst")
#FitDaughters_strange2Const.Verbose = True
##FitDaughters_strange2Const.OutputLevel = VERBOSE
#FitDaughters_strange2Const.UpdateDaughters = True
FitDaughters_strange2Const.UpdateDaughters = False
FitDaughters_strange2Const.daughtersToConstrain = ["D+","D_s-"]
FitDaughters_strange2Const.constrainToOriginVertex = False
FitDaughters_strange2Const.Substitutions = { "B0 -> (D+ -> K- pi+ pi+) ^(D- -> K+ K- pi-)" : "D_s-"}
#
##Constraint on daughters and constraint to OriginPV
FitDaughters_strange1PVConst = B2DDtuple.__getattr__("B0").addTupleTool("TupleToolDecayTreeFitterExtended/FitDsDPVConst")
#FitDaughters_strange1PVConst.Verbose = True
#FitDaughters_strange1PVConst.OutputLevel = VERBOSE
#FitDaughters_strange1PVConst.UpdateDaughters = True
FitDaughters_strange1PVConst.UpdateDaughters = False
FitDaughters_strange1PVConst.daughtersToConstrain = ["D_s+","D-"]
FitDaughters_strange1PVConst.constrainToOriginVertex = True
FitDaughters_strange1PVConst.Substitutions = { "B0 -> ^(D+ -> K- K+ pi+) (D- -> K+ pi- pi-)" : "D_s+"}
#
#
##Constraint on daughters and constraint to OriginPV
FitDaughters_strange2PVConst = B2DDtuple.__getattr__("B0").addTupleTool("TupleToolDecayTreeFitterExtended/FitDDsPVConst")
#FitDaughters_strange2PVConst.Verbose = True
##FitDaughters_strange2PVConst.OutputLevel = VERBOSE
#FitDaughters_strange2PVConst.UpdateDaughters = True
FitDaughters_strange2PVConst.UpdateDaughters = False
FitDaughters_strange2PVConst.daughtersToConstrain = ["D+","D_s-"]
FitDaughters_strange2PVConst.constrainToOriginVertex = True
FitDaughters_strange2PVConst.Substitutions = { "B0 -> (D+ -> K- pi+ pi+) ^(D- -> K+ K- pi-)" : "D_s-"}

# Particle substitutions in DTF
#FitSubstitution = B2DDtuple.__getattr__("B0").addTupleTool("TupleToolDecayTreeFitterExtended/FitSubstKpiminus")
#FitSubstitution.Verbose = True
#FitSubstitution.UpdateDaughters = True
#FitSubstitution.daughtersToConstrain = ["D-"]
#FitSubstitution.constrainToOriginVertex = True
#
#new_decay = "[B0 -> (D+ -> K- pi+ pi+) (D- -> ^K+ pi- pi-)]CC"
#dtf_substitution = { new_decay : "pi+"}
#FitSubstitution = B2DDtuple.__getattr__("B0").addTupleTool("TupleToolDecayTreeFitterExtended/FitSubstKpiplus")
#FitSubstitution.Verbose = True
#FitSubstitution.UpdateDaughters = True
#FitSubstitution.daughtersToConstrain = ["D+"]
#FitSubstitution.constrainToOriginVertex = True
#FitSubstitution.Substitutions = dtf_substitution

# if DaVinci().Simulation == False:
#   for input in B2DDtuple.Inputs:
#     if input not in refitter.Inputs:
#       print "Adding " + input + " to refitter"
#       refitter.Inputs.append(input)

###########################################################
seqB2DD                  = GaudiSequencer('seqB2DD')
TupleSeq                 = GaudiSequencer('TupleSeq')
seqB2DD.Members         += [seqDs]
seqB2DD.Members         += [B2DDtuple]
##############################################################################################
##### IMPORTS
##############################################################################################
TupleSeq.Members         += [seqB2DD]
TupleSeq.ModeOR           = True
TupleSeq.ShortCircuit     = False

from Configurables import EventTuple , TupleToolEventInfo
etuple = EventTuple()
etuple.ToolList += ["TupleToolEventInfo"]
######################################################################## 
# 
# DaVinci settings 
# 
subseq_annpid = GaudiSequencer('SeqANNPID')
from Configurables import ChargedProtoANNPIDConf
conf_ann_pid = ChargedProtoANNPIDConf('ANNPIDConf')
conf_ann_pid.RecoSequencer = subseq_annpid

DaVinci().Simulation = True
DaVinci().UserAlgorithms += [etuple , TupleSeq] 
DaVinci().EvtMax     = -1 
DaVinci().PrintFreq  = 5000 
DaVinci().InputType  = "DST" 
DaVinci().DataType   = '2012' 
from Configurables import CondDB 
CondDB( LatestGlobalTagByDataType = '2012') 
DaVinci().Lumi       = True 
########################################################################  

DaVinci().TupleFile = "B02DstD.root" 

#importOptions("./2016.py")
