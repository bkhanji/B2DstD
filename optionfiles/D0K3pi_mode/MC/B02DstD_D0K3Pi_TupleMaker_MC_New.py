"""
 Script to create a DecayTreeTuple for B2DD analysis:
"""
##############################################################################################
##### SETTINGS
##############################################################################################
import GaudiKernel.SystemOfUnits as Units
from Gaudi.Configuration import *
from DecayTreeTuple.Configuration import *
from Configurables import (BDecayTool,
                           BTagging,
                           BTaggingTool,
                           CheckPV,
                           CombineParticles,
                           DecayTreeTuple,
                           EventTuple,
                           FilterDesktop,
                           FitDecayTrees,
                           GaudiSequencer,
                           L0TriggerTisTos,
                           LoKi__Hybrid__TupleTool,
                           OfflineVertexFitter,
                           PrintDecayTree,
                           PrintDecayTreeTool,
                           TaggingUtilsChecker,
                           TESCheck,
                           TupleToolMCTruth,
                           TupleToolMCBackgroundInfo,
                           TriggerTisTos,
                           TupleToolAngles,
                           TupleToolDecay,
                           TupleToolDecayTreeFitterExtended,
                           TupleToolRecoStats,
                           TupleToolTagging,
                           TupleToolTISTOS,
                           TupleToolStripping,
                           TupleToolTrackPosition,
                           TupleToolTrigger,
                           TupleToolEventInfo,
                           TupleToolVtxIsoln,
                           TupleToolGeometry,
                           TupleToolTrackInfo,
                           TupleToolPid,
                           TupleToolANNPID,
                           TupleToolMassHypo)
import re
from PhysSelPython.Wrappers import Selection, SelectionSequence
from StandardParticles import StdLoosePions, StdAllNoPIDsPions
from Configurables import TrackScaleState
from Configurables import TrackSmearState as SMEAR
from Configurables import CondDB

from Configurables import DaVinci 
#DaVinci().RootInTES = '/Event/BhadronCompleteEvent' 

prefix = 'B2DD.Strip/'
#prefix = '/Event/BhadronCompleteEvent/'
##############################################################################################
##### IMPORTS
##############################################################################################
from PhysSelPython.Wrappers import Selection, SelectionSequence
from StandardParticles import StdLoosePions, StdAllNoPIDsPions
##############################################################################################
##### PRESELECTION
##############################################################################################
TupleSeq                 = GaudiSequencer('TupleSeq')
B02DstD_K3pi_InputLocation = prefix + "Phys/B02DstDD02K3PiBeauty2CharmLine/Particles" 
 
tuple_D0K3pi = DecayTreeTuple("D02K3piDKpipi") 
tuple_D0K3pi.Inputs = [B02DstD_K3pi_InputLocation ] 
tuple_D0K3pi.Decay = "[ [B0]cc -> ^( D*(2010)- -> ^pi- ^( [D~0]cc -> ^K+ ^pi- ^pi+ ^pi-) ) ^(D+ -> ^K- ^pi+ ^pi+) ]CC" 
tuple_D0K3pi.ToolList +=  [ 
    "TupleToolMCBackgroundInfo"
    ,"TupleToolGeometry"
    ,"TupleToolKinematic" 
    , "TupleToolPrimaries" 
    , "TupleToolRecoStats" 
     , "TupleToolTrackPosition" 
    , "LoKi::Hybrid::TupleTool/LoKiTool" 
    ] 
tuple_D0K3pi.ReFitPVs = True 
tuple_D0K3pi.OutputLevel = 3 

tuple_D0K3pi.Branches = { 
     "slowpi" : "[ [B0]cc ->  ( D*(2010)- ->  ^pi-  ( [D~0]cc ->  K+  pi-  pi+  pi- ) )  ( D+ ->  K-  pi+  pi+ ) ]CC" 
    ,"pi1_D0" : "[ [B0]cc ->  ( D*(2010)- ->   pi-  ( [D~0]cc ->  K+  pi-  pi+ ^pi- ) )  ( D+ ->  K-  pi+  pi+ ) ]CC" 
    ,"pi2_D0" : "[ [B0]cc ->  ( D*(2010)- ->   pi-  ( [D~0]cc ->  K+ ^pi-  pi+  pi- ) )  ( D+ ->  K-  pi+  pi+ ) ]CC" 
    ,"pi3_D0" : "[ [B0]cc ->  ( D*(2010)- ->   pi-  ( [D~0]cc ->  K+  pi- ^pi+  pi- ) )  ( D+ ->  K-  pi+  pi+ ) ]CC" 
    ,"K_D0"   : "[ [B0]cc ->  ( D*(2010)- ->   pi-  ( [D~0]cc -> ^K+  pi-  pi+  pi- ) )  ( D+ ->  K-  pi+  pi+ ) ]CC" 
    ,"D0"     : "[ [B0]cc ->  ( D*(2010)- ->   pi- ^( [D~0]cc ->  K+  pi-  pi+  pi- ) )  ( D+ ->  K-  pi+  pi+ ) ]CC" 
    ,"Dstar"  : "[ [B0]cc -> ^( D*(2010)- ->   pi-  ( [D~0]cc ->  K+  pi-  pi+  pi- ) )  ( D+ ->  K-  pi+  pi+ ) ]CC" 
    ,"pi1_Dp" : "[ [B0]cc ->  ( D*(2010)- ->   pi-  ( [D~0]cc ->  K+  pi-  pi+  pi- ) )  ( D+ ->  K-  pi+ ^pi+ ) ]CC" 
    ,"pi2_Dp" : "[ [B0]cc ->  ( D*(2010)- ->   pi-  ( [D~0]cc ->  K+  pi-  pi+  pi- ) )  ( D+ ->  K- ^pi+  pi+ ) ]CC" 
    ,"K_Dp"   : "[ [B0]cc ->  ( D*(2010)- ->   pi-  ( [D~0]cc ->  K+  pi-  pi+  pi- ) )  ( D+ -> ^K-  pi+  pi+ ) ]CC" 
    ,"Dp"     : "[ [B0]cc ->  ( D*(2010)- ->   pi-  ( [D~0]cc ->  K+  pi-  pi+  pi- ) ) ^( D+ ->  K-  pi+  pi+ ) ]CC" 
    ,"B0"     : "[ [B0]cc ->  ( D*(2010)- ->   pi-  ( [D~0]cc ->  K+  pi-  pi+  pi- ) )  ( D+ ->  K-  pi+  pi+ ) ]CC" 
    } 
tuple_D0K3pi.addTool(TupleToolDecay, name="B0")
from Configurables import MCMatchObjP2MCRelator
default_rel_locs = MCMatchObjP2MCRelator().getDefaultProperty('RelTableLocations')
rel_locs = [loc for loc in default_rel_locs if 'Turbo' not in loc]

MCTruth=tuple_D0K3pi.addTupleTool("TupleToolMCTruth")
MCTruth.addTool(MCMatchObjP2MCRelator)
MCTruth.MCMatchObjP2MCRelator.RelTableLocations = rel_locs
MCTruth.addTupleTool("MCTupleToolHierarchy")

######################################    
LoKi_B0=LoKi__Hybrid__TupleTool("LoKi_B0") 
LoKi_B0.Variables =  { 
    "ETA"             : "ETA", 
    "PHI"             : "PHI", 
    "DTF_TAU"         : "DTF_CTAU( 0, True )/0.299792458", 
    "DTF_CTAUS"       : "DTF_CTAUSIGNIFICANCE( 0, True )", 
    "DTF_CHI2NDOF"    : "DTF_CHI2NDOF( True )", 
    "DTF_MASS_3D"     : "DTF_FUN ( M , True , strings( ['D*(2010)+', 'D0' , 'D+'] ) )" , 
    "DTF_MASS_3DDs"   : "DTF_FUN ( M , True , strings( ['D*(2010)+', 'D0' , 'D_s+'] ) )" , 
    "DTF_VCHI2NDOF_3D": "DTF_FUN ( VFASPF(VCHI2/VDOF) , True , strings( ['D*(2010)+', 'D0' , 'D+'] ) )" , 
    "DTF_VCHI2NDOF_3D_Ds": "DTF_FUN ( VFASPF(VCHI2/VDOF) , True , strings( ['D*(2010)+', 'D0' , 'D_s+'] ) )" , 
    "DTF_MASS_DstD0"  : "DTF_FUN ( M , True , strings( ['D*(2010)+', 'D0'] ))" , 
    "DTF_MASS_DstDp"  : "DTF_FUN ( M , True , strings( ['D*(2010)+', 'D+'] ))" , 
    "DTF_MASS_DstDs"  : "DTF_FUN ( M , True , strings( ['D*(2010)+', 'D_s+'] ))" , 
    "DTF_MASS_D0Dp"   : "DTF_FUN ( M , True , strings( ['D0'      , 'D+'] ))" , 
    "DTF_MASS_D0Ds"   : "DTF_FUN ( M , True , strings( ['D0'      , 'D_s+'] ))" , 
    "DTF_MASS_D0"     : "DTF_FUN ( M , True , 'D0' )" , 
    "DTF_MASS_Dp"     : "DTF_FUN ( M , True , 'D+' )" , 
    "DTF_MASS_Dst"    : "DTF_FUN ( M , True , 'D*(2010)+' )" , 
    "DTF_VCHI2NDOF"   : "DTF_FUN ( VFASPF(VCHI2/VDOF) , True  )", 
    "DTF_VtxZ"        : "DTF_FUN ( BPVVDR , True ,strings( ['D*(2010)+', 'D0' , 'D+'] ) )", 
    "Rho_Dist"        : "BPVVDR", 
    "CORRM"           : "BPVCORRM", 
    "Costheta_star1"  : "LV01", 
    "DOCA"            : "DOCA(1,2)", 
    "DTF_CTAU"        : "DTF_CTAU( 0, True )", 
    "FDS"             : "BPVDLS" , 
    "COSPOL_D0"       : "COSPOL( '[[B0]cc -> (D*(2010)- ->  pi- ^([D~0]cc -> K+ pi- pi+ pi-)) (D+ -> K- pi+ pi+)]CC','[Beauty -> ^(D*(2010)- -> pi- ([D~0]cc -> K+ pi- pi+ pi-)) (D+ -> K- pi+ pi+)]CC' )", 
    "COSPOL_pi1_D0"   : "COSPOL( '[[B0]cc -> (D*(2010)- -> pi- ([D~0]cc -> K+ ^pi- pi+ pi-)) (D+ -> K- pi+ pi+)]CC','[Beauty -> (D*(2010)- -> pi- ^([D~0]cc -> K+ pi- pi+ pi-)) (D+ -> K- pi+ pi+)]CC' ) ", 
    "COSPOL_pi2_D0"   : "COSPOL( '[[B0]cc -> (D*(2010)- ->  pi-  ([D~0]cc -> K+ pi- ^pi+ pi-)) (D+ ->  K-  pi+  pi+)]CC' ,'[Beauty ->  (D*(2010)- -> pi- ^([D~0]cc -> K+ pi- pi+ pi-))  (D+ -> K- pi+ pi+)]CC' ) ", 
    "COSPOL_pi3_D0"   : "COSPOL( '[[B0]cc -> (D*(2010)- ->  pi-  ([D~0]cc -> K+ pi- pi+ ^pi-)) (D+ ->  K-  pi+  pi+)]CC' ,'[Beauty ->  (D*(2010)- -> pi- ^([D~0]cc -> K+ pi- pi+ pi-))  (D+ -> K- pi+ pi+)]CC' ) ", 
    "COSPOL_pi_Dst"   : "COSPOL( '[[B0]cc -> (D*(2010)- -> ^pi-  ([D~0]cc -> K+ pi- pi+ pi-)) (D+ ->  K-  pi+  pi+)]CC' ,'[Beauty -> ^(D*(2010)- -> pi-  ([D~0]cc -> K+ pi- pi+ pi-))  (D+ -> K- pi+ pi+)]CC' )", 
    "COSPOL_K_Dp"     : "COSPOL( '[[B0]cc -> (D*(2010)- ->  pi- ([D~0]cc -> K+ pi- pi+ pi-)) (D+ -> ^K-  pi+  pi+)]CC' ,'[Beauty ->  (D*(2010)- -> pi-  ([D~0]cc -> K+ pi- pi+ pi-)) ^(D+ -> K- pi+ pi+)]CC' ) ", 
    "COSPOL_pim_Dp"   : "COSPOL( '[[B0]cc -> (D*(2010)- ->  pi-  ([D~0]cc -> K+ pi- pi+ pi-)) (D+ ->  K-  pi+ ^pi+)]CC' ,'[Beauty ->  (D*(2010)- -> pi-  ([D~0]cc -> K+ pi- pi+ pi-)) ^(D+ -> K- pi+ pi+)]CC' ) ", 
    "COSPOL_pip_Dp"   : "COSPOL( '[[B0]cc -> (D*(2010)- ->  pi-  ([D~0]cc -> K+ pi- pi+ pi-)) (D+ ->  K- ^pi+  pi+)]CC' ,'[Beauty ->  (D*(2010)- -> pi- ([D~0]cc -> K+ pi- pi+ pi-)) ^(D+ -> K- pi+ pi+)]CC' )" 
}     
tuple_D0K3pi.B0.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_B0"] 
tuple_D0K3pi.B0.addTool(LoKi_B0) 

tuple_D0K3pi.addTool(TupleToolDecay, name="Dstar") 
tuple_D0K3pi.addTool(TupleToolDecay, name="D0") 
tuple_D0K3pi.addTool(TupleToolDecay, name="Dp") 

#from FlavourTagging.Tunings import TuneTool
#tt_tagging = tuple.addTupleTool("TupleToolTaggingMC")
#tt_tagging = tuple_D0K3pi.B0.addTupleTool("TupleToolTagging", name = "B0")
#tt_tagging.useFTonDST = True
#myTuneTool = TuneTool(tt_tagging,"Reco14_MC12","BTaggingTool")
#tt_tagging.Verbose = True

#For new FT tunning 
from Configurables import (BTaggingTool,
                           OSMuonTagger,
                           OSElectronTagger,
                           OSKaonTagger,
                           OSVtxChTagger)
from FlavourTagging.Tunings import applyTuning as applyFTTuning
tt_tagging3 = tuple_D0K3pi.addTupleTool("TupleToolTagging")
tt_tagging3.Verbose = True
tt_tagging3.AddMVAFeatureInfo = False
tt_tagging3.AddTagPartsInfo = False
tt_tagging3.OutputLevel = INFO
btagtool3 = tt_tagging3.addTool(BTaggingTool, name = "MyBTaggingTool3")
applyFTTuning(btagtool3, tuning_version="Summer2017Optimisation")
tt_tagging3.TaggingToolName = btagtool3.getFullName()
tt_tagging3.Verbose = True

LoKi_Dstar=LoKi__Hybrid__TupleTool("LoKi_Dstar")
LoKi_Dstar.Variables =  {
    "LOKI_MASS_D0"        : "DTF_FUN ( M , True , 'D0' )" ,
    "DTF_CTAU"            : "DTF_CTAU( 0, True )",
    "LOKI_DTF_VCHI2NDOF"  : "DTF_FUN ( VFASPF(VCHI2/VDOF) , True  )",
    "Dist_DTF_z"          : "DTF_FUN ( BPVVDR , True , 'D0')",
    "Dist_z"              : "BPVVDR",
    "CORRM"               : "BPVCORRM",
    "Costheta_star1"      : "LV01",
    "Costheta_star2"      : "LV02",
    "DOCA"                : "DOCA(1,2)"
        }

tuple_D0K3pi.Dstar.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_Dstar"] 
tuple_D0K3pi.Dstar.addTool(LoKi_Dstar) 
LoKi_D0=LoKi__Hybrid__TupleTool("LoKi_D0")
LoKi_D0.Variables =  {
    "DTF_CTAUS"       : "DTF_CTAUSIGNIFICANCE( 0, True )",
    "DTF_CTAU"        : "DTF_CTAU( 0, True )",
    "DTF_CHI2NDOF"    : "DTF_CHI2NDOF( True )",
    "FDS"             : "BPVDLS",
    "Dist_z"          : "BPVVDR",
    "Costheta_star0"  : "LV01",
    "CORRM"           : "BPVCORRM",
    "Costheta_star2"  : "LV02",
    "DOCA"            : "DOCA(1,2)",
    "M12"             : "MASS(1,2)",
    "M14"             : "MASS(1,4)",
    "M23"             : "MASS(2,3)",
    "M34"             : "MASS(3,4)"
        }
tuple_D0K3pi.D0.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_D0"] 
tuple_D0K3pi.D0.addTool(LoKi_D0) 

LoKi_Dplus=LoKi__Hybrid__TupleTool("LoKi_Dplus")
LoKi_Dplus.Variables =  {
    "DTF_CTAUS"       : "DTF_CTAUSIGNIFICANCE( 0, True )",
    "DTF_CTAU"        : "DTF_CTAU( 0, True )",
    "DTF_CHI2NDOF"    : "DTF_CHI2NDOF( True )",
    "FDS"             : "BPVDLS",
    "Inv_mass_12"     : "M12",
    "Inv_mass_13"     : "M13",
    "Inv_mass_23"     : "M23",
    "Dist_z"          : "BPVVDR",
    "Dist_DTF_z"      : "DTF_FUN(BPVVDR ,True , 'D0')",
    "Lc_mass1"        : "WM('K-' ,'p+'  , 'pi+')" ,
    "Lc_mass2"        : "WM('K-' ,'pi+' , 'p+')" ,
    "Ds_mass1"        : "WM('K-' ,'K+'  , 'pi+')" ,
    "Ds_mass2"        : "WM('K-' ,'pi+'  , 'K+')" ,
    "phi_mass"        : "WM('K-' ,'K+')",
    "Costheta_star0"  : "LV01",
    "Costheta_star2"  : "LV02",
    "Costheta_star3"  : "LV03",
    "DOCA1"           : "DOCA(1,2)",
    "DOCA2"           : "DOCA(3,2)",
    "DOCA3"           : "DOCA(1,3)",
    "CORRM"           : "BPVCORRM"
    }
tuple_D0K3pi.Dp.ToolList+=["LoKi::Hybrid::TupleTool/LoKi_Dplus"] 
tuple_D0K3pi.Dp.addTool(LoKi_Dplus) 

hlt1_lines = ['L0PhysicsDecision',
                            'L0MuonDecision',
                            'L0DiMuonDecision',
                            'L0MuonHighDecision',
                            'L0HadronDecision',
                            'L0ElectronDecision',
                            'L0PhotonDecision',
                            'Hlt1TrackAllL0Decision',
                            'Hlt1TrackAllL0TightDecision',
                            'Hlt1L0AnyDecision',
                            'Hlt1GlobalDecision',
                            "Hlt1TrackAllL0Decision",
                            "Hlt1TrackMVADecision",
                            "Hlt1TwoTrackMVADecision"
                            ]

hlt2_lines = ['Hlt2GlobalDecision'
                            ,"Hlt2Topo2BodyDecision"
                            ,"Hlt2Topo3BodyDecision"
                            ,"Hlt2Topo4BodyDecision"
                            ,'Hlt2Topo2BodyBBDTDecision'
                            ,'Hlt2Topo3BodyBBDTDecision'
                            ,'Hlt2Topo4BodyBBDTDecision'
                            ,'Hlt2IncPhiDecision'
                            ,'Hlt2IncPhiSidebandsDecision']

trigger_lines = hlt1_lines + hlt2_lines

TupleToolTISTOS = TupleToolTISTOS('TupleToolTISTOS')
TupleToolTISTOS.TriggerList= trigger_lines
TupleToolTISTOS.VerboseL0 = True
TupleToolTISTOS.VerboseHlt1 = True
TupleToolTISTOS.VerboseHlt2 = True
tt_trigger = tuple_D0K3pi.addTupleTool("TupleToolTrigger")
tt_trigger.Verbose = True
tt_trigger.TriggerList = trigger_lines
tt_tistos = tuple_D0K3pi.B0.addTupleTool("TupleToolTISTOS")
tt_tistos.Verbose = True
tt_tistos.TriggerList = trigger_lines
tuple_D0K3pi.B0.addTool(TupleToolTISTOS , name = 'TupleToolTISTOS' )
tuple_D0K3pi.B0.ToolList   += [ "TupleToolTISTOS"]

tt_trinfo = tuple_D0K3pi.addTupleTool(TupleToolTrackInfo)
tt_trinfo.Verbose = True

TupleSeq.Members        += [ tuple_D0K3pi ] 
TupleSeq.ModeOR          = True 
TupleSeq.ShortCircuit    = False 
######################################################################## 
tuple_D02K3piDKKpi        =  tuple_D0K3pi.clone("D02K3piDKKpi") 
tuple_D02K3piDKKpi.Decay  = "[ [B0]cc -> ^( D*(2010)- -> ^pi- ^( [D~0]cc -> ^K+ ^pi- ^pi+ ^pi-) ) ^(D+ -> ^K- ^K+ ^pi+) ]CC" 
tuple_D02K3piDKKpi.Inputs =  [B02DstD_K3pi_InputLocation] 
tuple_D02K3piDKKpi.Branches = { 
     "slowpi" : "[ [B0]cc ->  ( D*(2010)- ->  ^pi-  ( [D~0]cc ->  K+  pi-  pi+  pi- ) )  ( D+ ->  K-  K+  pi+ ) ]CC" 
    ,"pi1_D0" : "[ [B0]cc ->  ( D*(2010)- ->   pi-  ( [D~0]cc ->  K+  pi-  pi+ ^pi- ) )  ( D+ ->  K-  K+  pi+ ) ]CC" 
    ,"pi2_D0" : "[ [B0]cc ->  ( D*(2010)- ->   pi-  ( [D~0]cc ->  K+ ^pi-  pi+  pi- ) )  ( D+ ->  K-  K+  pi+ ) ]CC" 
    ,"pi3_D0" : "[ [B0]cc ->  ( D*(2010)- ->   pi-  ( [D~0]cc ->  K+  pi- ^pi+  pi- ) )  ( D+ ->  K-  K+  pi+ ) ]CC" 
    ,"K_D0"   : "[ [B0]cc ->  ( D*(2010)- ->   pi-  ( [D~0]cc -> ^K+  pi-  pi+  pi- ) )  ( D+ ->  K-  K+  pi+ ) ]CC" 
    ,"D0"     : "[ [B0]cc ->  ( D*(2010)- ->   pi- ^( [D~0]cc ->  K+  pi-  pi+  pi- ) )  ( D+ ->  K-  K+  pi+ ) ]CC" 
    ,"Dstar"  : "[ [B0]cc -> ^( D*(2010)- ->   pi-  ( [D~0]cc ->  K+  pi-  pi+  pi- ) )  ( D+ ->  K-  K+  pi+ ) ]CC" 
    ,"pi1_Dp" : "[ [B0]cc ->  ( D*(2010)- ->   pi-  ( [D~0]cc ->  K+  pi-  pi+  pi- ) )  ( D+ ->  K-  K+ ^pi+ ) ]CC" 
    ,"pi2_Dp" : "[ [B0]cc ->  ( D*(2010)- ->   pi-  ( [D~0]cc ->  K+  pi-  pi+  pi- ) )  ( D+ ->  K- ^K+  pi+ ) ]CC" 
    ,"K_Dp"   : "[ [B0]cc ->  ( D*(2010)- ->   pi-  ( [D~0]cc ->  K+  pi-  pi+  pi- ) )  ( D+ -> ^K-  K+  pi+ ) ]CC" 
    ,"Dp"     : "[ [B0]cc ->  ( D*(2010)- ->   pi-  ( [D~0]cc ->  K+  pi-  pi+  pi- ) ) ^( D+ ->  K-  K+  pi+ ) ]CC" 
    ,"B0"     : "[ [B0]cc ->  ( D*(2010)- ->   pi-  ( [D~0]cc ->  K+  pi-  pi+  pi- ) )  ( D+ ->  K-  K+  pi+ ) ]CC" 
    } 
TupleSeq.Members         += [ tuple_D02K3piDKKpi ] 
TupleSeq.ModeOR           = True 
TupleSeq.ShortCircuit     = False 

from Configurables import EventTuple , TupleToolEventInfo
etuple = EventTuple()
etuple.ToolList += ["TupleToolEventInfo"]
######################################################################## 
# 
# DaVinci settings 
# 
DaVinci().EvtMax     = -1
DaVinci().Lumi       = False
DaVinci().Simulation = True

DaVinci().InputType  = 'DST'
DaVinci().DataType   = "2016"
CondDB( LatestGlobalTagByDataType = "2016" )
DaVinci().TupleFile       = "DTT.root" # Ntuple

subseq_annpid = GaudiSequencer('SeqANNPID')

from Configurables import ChargedProtoANNPIDConf
conf_ann_pid = ChargedProtoANNPIDConf('ANNPIDConf')
conf_ann_pid.RecoSequencer = subseq_annpid

#DaVinci().UserAlgorithms += [seqB2DstD]
DaVinci().UserAlgorithms += [ etuple , TupleSeq ] 
DaVinci().MainOptions  = ""  

DaVinci().EvtMax     = -1 
DaVinci().PrintFreq  = 5000 
DaVinci().InputType  = "DST" 
DaVinci().DataType   = '2016' 
from Configurables import CondDB 
CondDB().UseLatestTags = ['2016'] 
DaVinci().Lumi       = True 
########################################################################  
'''
DaVinci().TupleFile = "B02DstD.root" 
from GaudiConf import IOHelper
IOHelper('ROOT').inputFiles(
    [
    '/eos/lhcb/user/b/bkhanji/MC/B2DstD/00058549_00000001_1.b2dd.strip.dst'
    ], clear=True)
'''
