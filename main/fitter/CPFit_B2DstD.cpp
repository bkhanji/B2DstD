#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <boost/lexical_cast.hpp>


// from ROOT
#include "TCanvas.h"
#include "TFile.h"
#include "TStyle.h"
#include "TTree.h"
#include "TROOT.h"
#include "TLatex.h"
#include "TList.h"
#include "TPad.h"
#include "TMath.h"
#include "TRegexp.h"
#include "TObjArray.h"
#include "TH1.h"

// from RooFit
#include "RooAbsPdf.h"
#include "RooArgSet.h"
#include "RooArgList.h"
#include "RooBinning.h"
#include "RooCmdArg.h"
#include "RooConstVar.h"
#include "RooDataSet.h"
#include "RooFitResult.h"
#include "RooGaussian.h"
#include "RooExponential.h"
#include "RooWorkspace.h"
#include "RooFormulaVar.h"
#include "RooKeysPdf.h"
#include "RooChebychev.h"
#include "RooCategory.h"
#include "RooGenericPdf.h"
#include "RooDecay.h"
#include "RooBDecay.h"
#include "RooEffProd.h"
#include "RooProdPdf.h"
#include "RooAddPdf.h"
#include "RooGaussModel.h"
#include "RooAddModel.h"
#include "RooPlot.h"
#include "RooRealVar.h"
#include "RooCBShape.h"
#include "RooDataHist.h"
#include "RooHistPdf.h"
#include "RooBinning.h"
#include "RooAbsBinning.h"
#include "RooHistFunc.h"
#include "RooUnblindUniform.h"
#include "P2VV/RooCubicSplineFun.h"
#include "P2VV/RooCubicSplineKnot.h"
#include "P2VV/RooGaussEfficiencyModel.h"
#include "P2VV/RooEffResAddModel.h"

//from doosoftware
#include "doocore/io/MsgStream.h"
#include "doocore/io/EasyTuple.h"
#include "doofit/builder/EasyPdf/EasyPdf.h"
#include "doocore/lutils/lutils.h"
#include "doofit/roofit/pdfs/DooCubicSplinePdf.h"
#include "doofit/fitter/easyfit/EasyFit.h"
#include "doofit/fitter/easyfit/FitResultPrinter.h"
#include "Urania/DecRateCoeff.h"


using namespace std;
using std::cout;
using std::endl;
using namespace RooFit;
using namespace doocore;
using namespace doocore::io;
using namespace doofit::builder;
 
int main() { 

	MsgStream mymsgstream(kTextBlue);
	mymsgstream << "Let's start" << endmsg;
 
 	//Observables
  RooRealVar obsTime("obsTime","#it{t}[ps]", 0.3, 12.3);
  //RooRealVar B0_TAGOMEGA ("B0_TAGOMEGA", "B0_TAGOMEGA", 0, 0.5);
  RooRealVar Sig_yield_sw ("Sig_yield_sw", "Sig_yield_sw", -300, 200);
  RooRealVar obsEtaOS("obsEtaOS","#it{#eta}_{OS}",0,0.5);
  RooCategory obsTagOS("obsTagOS", "#it{d}_{OS}");
  obsTagOS.defineType("B0" , +1);
  obsTagOS.defineType("B0b", -1);
  obsTagOS.defineType("none", 0);

  // Endzustand, zwei Typen in der Kategorie
  RooCategory catFinalState("catFinalState","catFinalState");
  catFinalState.defineType("Dst-D+",+1);
  catFinalState.defineType("Dst+D-",-1);

 	// Inputfile and dataset
  EasyTuple etuple("/net/storage03/data/users/mschellenberg/tupel/B02DstD/Data/DT20112012_B02DstD_Stripping21r0r1_DVv36r5_combined_20150721_mschellenberg_TupleA_flat_bestPV_addedBranch_MassHypo_forMasterthesis_preselected__BDTapplied_Veto_PID_randomCand_sWeights_finalstate.root","DecayTree",RooArgSet(obsTime,obsTagOS,catFinalState,obsEtaOS,Sig_yield_sw));
  RooDataSet &data = etuple.ConvertToDataSet(WeightVar("Sig_yield_sw"));
  
  // MC
  // RooCategory catBkgCat("B0_BKGCAT","B0_BKGCAT");
  // catBkgCat.defineType("Signal",0);
  // EasyTuple etuple("/net/storage03/data/users/mschellenberg/tupel/B02DstD/MC/MC_Sim08g_2012_Pythia8_B02DstD_Stripping21_DVv36r5_mschellenberg_20150721_NoPrescalingFlagged_combined_flat_bestPV_finalstate.root","DecayTree",RooArgSet(obsTime,obsTagOS,catFinalState,obsEtaOS,catBkgCat));  
  // RooDataSet &data = etuple.ConvertToDataSet();

  cout << "data:" << endl;
  data.Print("v");

	// WS to store easy pdf info
	RooWorkspace* ws = new RooWorkspace();
	EasyPdf epdf(ws);


	// Decay Time Acceptance
  std::vector<double> time_knots;
  time_knots.push_back(0.5);
  time_knots.push_back(1);
  time_knots.push_back(2);
  time_knots.push_back(8);
	
	RooArgList listofsplinecoefficients("listofsplinecoefficients");
	RooRealVar* parSigTimeAccCSpline;
	for (int i = 1; i <= (time_knots.size()+2) ; i++) {
		std::string binname = "parSigTimeAccCSpline" + boost::lexical_cast<std::string>(i);
		parSigTimeAccCSpline = new RooRealVar(binname.c_str(),binname.c_str(),1,0,2);
		listofsplinecoefficients.add(*parSigTimeAccCSpline);
	}


	RooCubicSplineFun accspline("accspline","Spline Acceptance",obsTime,time_knots,listofsplinecoefficients);

  RooGaussEfficiencyModel gaussefficiencymodel1("gaussefficiencymodel1","mean resolution model",obsTime,accspline,RooConst(0),RooConst(0.099217));
  RooGaussEfficiencyModel gaussefficiencymodel2("gaussefficiencymodel2","mean resolution model",obsTime,accspline,RooConst(0),RooConst(0.044552));
  RooRealVar gaussfraction("gaussfraction","gaussfraction",0.17927);
  RooEffResAddModel gaussefficiencymodel("gaussefficiencymodel","efficiency model",RooArgList(gaussefficiencymodel1,gaussefficiencymodel2),gaussfraction);

  //============================================================================
  // Flavour Tagging 
  // ----bauen der eta-pdf, ich habe hier eine CubicSplines genutzt----
  std::vector<double> eta_knots;
  eta_knots.push_back(0.07);
  eta_knots.push_back(0.11);
  eta_knots.push_back(0.138);
  eta_knots.push_back(0.145);
  eta_knots.push_back(0.16);
  eta_knots.push_back(0.23);
  eta_knots.push_back(0.28);
  eta_knots.push_back(0.30);
  eta_knots.push_back(0.35);
  eta_knots.push_back(0.42);
  eta_knots.push_back(0.44);
  eta_knots.push_back(0.48);
  eta_knots.push_back(0.5);

  // empty arg list for coefficients
  RooArgList* list = new RooArgList();

  // create first coefficient
  RooRealVar* coeff_first = &(epdf.Var("parCSpline1"));
  coeff_first->setRange(0,10000);
  list->add( *coeff_first );

  for (unsigned int i=1; i <= eta_knots.size(); ++i){
    std::string number = boost::lexical_cast<std::string>(i);
    RooRealVar* coeff = &(epdf.Var("parCSpline"+number));
    coeff->setRange(0,10000);
    list->add( *coeff );
  }

  // create last coefficient
  RooRealVar* coeff_last = &(epdf.Var("parCSpline"+boost::lexical_cast<std::string>(eta_knots.size())));
  coeff_last->setRange(0,10000);
  list->add( *coeff_last );
  doofit::roofit::pdfs::DooCubicSplinePdf pdfEta("pdfEta",epdf.Var("obsEtaOS"),eta_knots,*list,0,0.5);

  // Parameter
  // ----anlegen der kalibrationsfunktion fürs FT, nutze eine RooFormulaVar----
  epdf.Var("parOmegaMean");  // aka p0
  epdf.Var("parOmegaSlope"); // aka p1
  epdf.Var("parEtaMean");
  epdf.Formula("varOmegaCalib","@0+@1*(@2-@3)",RooArgList(epdf.Var("parOmegaMean"),epdf.Var("parOmegaSlope"),epdf.Var("obsEtaOS"),epdf.Var("parEtaMean")));


  epdf.Var("parTagEff");     //("parTagEff","parTagEff",0.3,0.,1.);
  epdf.Var("parDeltaGamma"); //("parDeltaGamma","#Delta#it{#Gamma}",0.);
  epdf.Var("parDeltaM");     //("parDeltaM","#Delta#it{m}",0.510);
  epdf.Var("parProdAsym");   //("parProdAsym","parProdAsym",0.);
  epdf.Var("parDetAsym");    //("parDetAsym","parDetAsym",0.);
  epdf.Var("parTagEffAsym"); //("parTagEffAsym","parTagEffAsym",0.);

  epdf.Var("parCavg");   // RooRealVar parCavg("parCavg","#it{C}",-0.01,-1.,1.);
  epdf.Var("parCdelta"); // RooRealVar parCdelta("parCdelta","#Delta#it{C}",0.,-1.,1.);
  epdf.Var("parSavg");   // RooRealVar parSavg("parSavg","#it{S}",-0.78,-1.,1.);
  epdf.Var("parSdelta"); // RooRealVar parSdelta("parSdelta","#Delta#it{S}",0.,-1.,1.);
  epdf.Var("parChavg");  // RooRealVar parChavg("parChavg","#it{C}",1.);
  epdf.Var("parChdelta");// RooRealVar parChdelta("parChdelta","#Delta#it{C}",0.);
  epdf.Var("parShavg");  // RooRealVar parShavg("parShavg","#it{S}",0.);
  epdf.Var("parShdelta");// RooRealVar parShdelta("parShdelta","#Delta#it{S}",0.);



  // ----die eigentlichen DecRateCoeff's----
  DecRateCoeff *coeff_c  = new DecRateCoeff("coef_cos","coef_cos"  ,DecRateCoeff::Flags(0b101),catFinalState,obsTagOS,epdf.Var("parCavg") ,epdf.Var("parCdelta") ,obsEtaOS,pdfEta,epdf.Var("parTagEff"),epdf.Real("varOmegaCalib"),epdf.Var("parProdAsym"),epdf.Var("parDetAsym"),epdf.Var("parTagEffAsym"));
  DecRateCoeff *coeff_s  = new DecRateCoeff("coef_sin","coef_sin"  ,DecRateCoeff::Flags(0b111),catFinalState,obsTagOS,epdf.Var("parSavg") ,epdf.Var("parSdelta") ,obsEtaOS,pdfEta,epdf.Var("parTagEff"),epdf.Real("varOmegaCalib"),epdf.Var("parProdAsym"),epdf.Var("parDetAsym"),epdf.Var("parTagEffAsym"));
  DecRateCoeff *coeff_sh = new DecRateCoeff("coef_sinh","coef_sinh",DecRateCoeff::Flags(0b100),catFinalState,obsTagOS,epdf.Var("parShavg"),epdf.Var("parShdelta"),obsEtaOS,pdfEta,epdf.Var("parTagEff"),epdf.Real("varOmegaCalib"),epdf.Var("parProdAsym"),epdf.Var("parDetAsym"),epdf.Var("parTagEffAsym"));
  DecRateCoeff *coeff_ch = new DecRateCoeff("coef_cosh","coef_cosh",DecRateCoeff::Flags(0b100),catFinalState,obsTagOS,epdf.Var("parChavg"),epdf.Var("parChdelta"),obsEtaOS,pdfEta,epdf.Var("parTagEff"),epdf.Real("varOmegaCalib"),epdf.Var("parProdAsym"),epdf.Var("parDetAsym"),epdf.Var("parTagEffAsym"));

	//RooDecay pdf_time("pdf_time", "pdf_time", obsTime, epdf.Var("parTau"), gaussefficiencymodel, RooDecay::SingleSided);
  RooBDecay pdf_time("pdf_time","pdf_time",
                     obsTime,epdf.Var("parTau"),epdf.Var("parDeltaGamma"),
                     *coeff_ch,*coeff_sh,*coeff_c,*coeff_s,
                     epdf.Var("parDeltaM"), 
                     gaussefficiencymodel,   
                     RooBDecay::SingleSided);





	// Configuration files
 	pdf_time.getParameters(&data)->writeToFile("../main/fitter/ObsAndPars/CPFit/parameters.txt.new");
	pdf_time.getObservables(&data)->writeToFile("../main/fitter/ObsAndPars/CPFit/observables.txt.new");
  pdf_time.getParameters(&data)->readFromFile("../main/fitter/ObsAndPars/CPFit/parameters.txt");
  pdf_time.getObservables(&data)->readFromFile("../main/fitter/ObsAndPars/CPFit/observables.txt");


	ws->Print("V");

  //Fitting
  doofit::fitter::easyfit::EasyFit efit("fitter");

  efit.SetPdfAndDataSet(&pdf_time,&data);
  //options to control construction of fit
  efit.SetNumCPU(10);
  efit.SetOffset(false);
  efit.SetExtended(false);
  //options to control flow of fit procedure
  efit.SetHesseInit(false);
  efit.SetHesse(true);
  efit.SetMinos(false);
  efit.SetStrategy(2);
  efit.SetOptimize(true);
  efit.SetSumW2Error(false);
  //set output level of fit
  efit.SetPrintLevel(1);
  efit.SetWarnings(false);
  efit.SetNumEvalErrors(-1);
  efit.Fit();
  const RooFitResult *fitresult = efit.GetFitResult();
  doofit::fitter::easyfit::FitResultPrinter fit_result_printer(*fitresult);
  fit_result_printer.set_print_const_pars(true);
  fit_result_printer.set_full_precision_printout(true);
  fit_result_printer.Print();
  TMatrixTSym<double> corMatrix = fitresult->correlationMatrix();
  corMatrix.Print();


	pdf_time.getParameters(&data)->writeToFile("Results/CPFit_B2DstD/FitResults.txt");

  lutils::setStyle();
  TCanvas c("c","c",800,600); 

 	RooPlot* timeFrame = obsTime.frame(Name("timeFrame"),Title("Bd_Tau fit"), Bins(50));
  data.plotOn(timeFrame); 
 	pdf_time.plotOn(timeFrame);  
	timeFrame->SetMinimum(0.1);

  lutils::PlotPulls("TimeFit_Bd", timeFrame, "Results/CPFit_B2DstD/", false, false);
  lutils::PlotPulls("TimeFit_Bd_log", timeFrame, "Results/CPFit_B2DstD/", true, false);

  RooPlot* etaFrame = obsEtaOS.frame(Name("etaFrame"),Title("Eta fit"), Bins(50));
  data.plotOn(etaFrame,Cut("obsTagOS!=0")); 
  pdfEta.plotOn(etaFrame);  
  etaFrame->SetMinimum(0.1);

  lutils::PlotPulls("Eta", etaFrame, "Results/CPFit_B2DstD/", false, false);
  lutils::PlotPulls("Eta_log", etaFrame, "Results/CPFit_B2DstD/", true, false);


	//cout << "Mean eta: " << data.mean(B0_TAGOMEGA) << endl;

//
//	RooPlot* plot = obsTime.frame();
//	plot->GetXaxis()->SetTitle("#it{B^{0}} Zerfallszeit [ps]");
//
//	data.plotOn(plot);
//	pdf_time.plotOn(plot);
//	lutils::PlotPulls("TimeFit", plot,  "/home/mschellenberg/Master/main/B02DstD/Fitter/DecayTime/Results/DecayTimeFit/", true, false);

 	c.SetLogx(false);
 	RooPlot* frame = obsTime.frame(0.3, 6.3);
 	const RooCubicSplineFun spline = dynamic_cast<const RooCubicSplineFun&>(accspline);
 	spline.plotOn(frame);

  RooArgList coeffs = spline.coefficients();
  
  RooArgList* baselist;
  
  std::vector<RooCubicSplineFun> basesplines;
  
  for(int j=0; j<coeffs.getSize(); j++){
    	baselist = (RooArgList*)coeffs.snapshot();
    	for(int i=0; i<baselist->getSize(); i++){
      	if(j == i){
        		(*baselist)[i] = coeffs[i];
      	}
      	else{
        		*((RooRealVar*)(&((*baselist)[i]))) = (0.0);
      	}
    	}
    	baselist->Print();
    	RooCubicSplineFun basespline(("csplines"+boost::lexical_cast<std::string>(j)).c_str(),
      	                         ("csplines"+boost::lexical_cast<std::string>(j)).c_str(),
          	                     dynamic_cast<RooRealVar&>(obsTime), time_knots, *baselist);
    	basesplines.push_back(basespline);
    	basespline.Print();
    	basesplines.back().plotOn(frame, RooFit::LineStyle(2));
  }
  frame->Draw();

 	c.SaveAs("Results/CPFit_B2DstD/splines.pdf");
}


