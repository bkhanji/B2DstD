

from ROOT import *


TString eosMC_dir  = "root://eoslhcb.cern.ch//eos/lhcb/wg/semileptonic/Bs2KmunuAna/Tuples/Bs2KMuNu_MCTUPLES_RAW_BDT_30May16/"; 

TString BCut = "Bs_MCORR < 7000 && Bs_MCORR > 2500";
TString KCut = "(kaon_m_PIDK - kaon_m_PIDp) > 5 && kaon_m_PIDK > 5 && (kaon_m_PIDK - kaon_m_PIDmu) > 5 && kaon_m_P > 10000 && kaon_m_PT > 500";
TString Preselection = "(Bs_Hlt2SingleMuonDecision_TOS || Bs_Hlt2TopoMu2BodyBBDTDecision_TOS == 1) && Bs_Q2SOL1!=-1000 && Bs_M > 1900 && totCandidates<2";
TString Mpi0 = "(kaon_m_Pi0_M>112 && kaon_m_Pi0_M<158)";
TString kstar = "(kaon_m_MasshPi0>832 && kaon_m_MasshPi0<962) && " + Mpi0;
TString kstar1 = "(kaon_m_MasshPi0>1340 && kaon_m_MasshPi0<1520) && " + Mpi0;
TString myKstar_veto = "!("+kstar+" || "+kstar1+")";

#
def DefineImportantVariables( mytree , MCflag ):

    mytree.SetBranchStatus("*",0);
    mytree.SetBranchStatus("Bs_M",1);
    mytree.SetBranchStatus("Bs_MCORR",1);
    mytree.SetBranchStatus("Bs_P*",1);
    return 
#
def PrepareTrees( data_PATH , MC_flag )
{
  OSdata_2012_tree = TChain("data_OS","data_OS");
  OSdata_2012_tree.Add( OS_data);
  DefineImportantVariables(OSdata_2012_tree );
  newOSdata_2012_tree = OSdata_2012_tree.CopyTree(Cuts_t,"", 1000000000 , 0)  
  return newOSdata_2012_tree 
#

void CompareVars( TTree *newOSdata_2012_tree=0  , TTree *newSSdata_2012_tree=0  ) {
  
  cout <<"Same Sign Entries: "<< newSSdata_2012_tree.GetEntries() << endl;
  cout <<"Opposite Sign Entries: "<< newOSdata_2012_tree.GetEntries() << endl;
  cout<<" 2012/2011 ~ " <<setprecision(2)<<(double) newSSdata_2012_tree.GetEntries()/newOSdata_2012_tree.GetEntries() << endl;
  
  // -- Get list of branches for the tree
  TObjArray *o_2012 = newSSdata_2012_tree.GetListOfBranches(); 
  TObjArray *o_2011 = newOSdata_2012_tree.GetListOfBranches(); 
  
  int m = o_2012.GetEntries(); 
  cout << "Number of branches: " << m << endl;
  
  TCanvas **c      = new TCanvas*[m];
  TCanvas **c_div  = new TCanvas*[m];
  TH1D** array_SS  = new TH1D*[m];
  TH1D** array_OS  = new TH1D*[m];
  TH1D** array_div = new TH1D*[m];
  
  for (int ttt=0;ttt<m;ttt++) {
    cout<<"Name of the Branch is : " <<((TBranch*)(*o_2012)[ttt]).GetName() <<endl;
    //cout<<"Minimum is : " << SSdata_2012_tree.GetMinimum(((TBranch*)(*o_2012)[ttt]).GetName())<<endl;
    //cout<<"Maximum is : " << SSdata_2012_tree.GetMaximum(((TBranch*)(*o_2012)[ttt]).GetName())<<endl;
    array_SS[ttt] = new TH1D(Form("SS_2012_%d",ttt),((TBranch*)(*o_2012)[ttt]).GetName(),100 ,
                             newSSdata_2012_tree.GetMinimum(((TBranch*)(*o_2012)[ttt]).GetName()) ,
                             newSSdata_2012_tree.GetMaximum(((TBranch*)(*o_2012)[ttt]).GetName()) );
    
    array_OS[ttt] = new TH1D(Form("OS_2012_%d",ttt),((TBranch*)(*o_2012)[ttt]).GetName(),100 ,
                             newSSdata_2012_tree.GetMinimum(((TBranch*)(*o_2012)[ttt]).GetName()) ,
                             newSSdata_2012_tree.GetMaximum(((TBranch*)(*o_2012)[ttt]).GetName()) );

  }
  // -- Loop over all, and draw their variables into TCanvas c1
  int cnt(0); 
  
  for (int i = 0; i < m; ++i) {
    newSSdata_2012_tree.Project(array_SS[i].GetName(), ((TBranch*)(*o_2012)[i]).GetName(),  
                              MyCuts
                              ,"" , 10000000000 , 0 ) ; 
    //array_SS[i].Scale(10);
    array_OS[i].SetLineColor(kRed);
    newOSdata_2012_tree.Project(array_OS[i].GetName(), ((TBranch*)(*o_2012)[i]).GetName(), 
                              MyCuts
                              ,"" , 100000000 , 0   ) ; 
    c[i]= new TCanvas( "c_"+ (TString)((TBranch*)(*o_2012)[i]).GetName() , 
                       "c_"+ (TString)((TBranch*)(*o_2012)[i]).GetName() ); //
    c[i].cd() ;
    arrangehistos( array_OS[i]  , array_SS[i]  ,  (TString)((TBranch*)(*o_2012)[i]).GetName()  ,  c[i]  );
    c[i].SaveAs((TString)c[i].GetName()+".pdf");
    // array_OS[i].DrawNormalized();
    // array_SS[i].DrawNormalized("same");
    //c[i].Update(); 

    c_div[i] = new TCanvas( Form("c_div%d", i ) , Form("c_div%d", i ) );
    array_OS[i].Divide(array_SS[i] );
    c_div[i].cd() ;
    array_OS[i].Draw();
    //c_div[i].Update();
  }
  return ;   

}

void GetSSCrossOS( TTree *newOSdata_2012_tree=0  , TTree *newSSdata_2012_tree =0  )
{
  
  UInt_t runN_OS ,runN_SS;
  ULong64_t EvtN_OS,EvtN_SS;
  
  newOSdata_2012_tree.SetBranchAddress("eventNumber"  , &EvtN_OS  );
  newOSdata_2012_tree.SetBranchAddress("runNumber"  , &runN_OS  );
  newSSdata_2012_tree.SetBranchAddress("eventNumber"  , &EvtN_SS  );
  newSSdata_2012_tree.SetBranchAddress("runNumber"  , &runN_SS  );
  
  TFile *Crossfile = new TFile("CrossOSandSS.root","recreate");
  TTree *Crosstree = newSSdata_2012_tree.CloneTree(0);
  
  Long64_t nentries_OS = newOSdata_2012_tree.GetEntries();
  Long64_t nentries_SS = newSSdata_2012_tree.GetEntries();
  int offset = 0;
  
  for (Long64_t i=0;i< nentries_OS;i++) 
  {
    newOSdata_2012_tree.GetEntry(i);
    for (Long64_t i_SS =0 ; i_SS< newSSdata_2012_tree.GetEntries() ; i_SS++) 
    {
      newSSdata_2012_tree.GetEntry(i_SS + offset);
      if (runN_OS == runN_SS && EvtN_OS == EvtN_SS)
      {
        cout<<"==============================="<<endl;
        cout<<" OS index : " << i <<endl;
        cout<<" SS index : " << i_SS <<endl;
        cout<<"  Cross event found ! "  << endl;
        cout<< " runN_OS = " << runN_OS << endl ;
        cout<< " runN_SS = " << runN_SS << endl ;
        cout<< " EvtN_OS = " << EvtN_OS << endl ;
        cout<< " EvtN_SS = " << EvtN_SS << endl ;
        cout<<"------------------------------"<<endl;
        Crosstree.Fill();
        offset = i_SS;
        
        break;
      }
    }
  }
  
  Crosstree.AutoSave();
  Crosstree.Print();
  Crosstree.Write();
  Crossfile.Close();
  
  cout<<"............................................................"<<endl;
  cout<<"............................................................"<<endl;
  cout<<" OS nentries    : "<< newOSdata_2012_tree.GetEntries() << endl;
  cout<<" SS nentries    : "<< newSSdata_2012_tree.GetEntries() << endl;
  cout<<" Cross nentries : "<< Crosstree          .GetEntries() << endl;
  cout<<"------------------------------------------------------------"<<endl;
  cout<<"------------------------------------------------------------"<<endl;
    
  return ;
}

TH1D *GetMyVarHisto(TTree *newOSdata_2012_tree,TString MyVar="",TString MyVar_h_name="", TString Myadd_Cut = "", double min_d =0 , double max_d=0) //
{
  double pion_mass = 139.57;
  double kaon_mass = 493.67;
  
  TH1D *InvMofBplus_h = new TH1D(MyVar_h_name, MyVar_h_name,100 , min_d, max_d);
  InvMofBplus_h.Sumw2();
  
  newOSdata_2012_tree.Project( InvMofBplus_h.GetName(), MyVar , MyCuts + " && " + Myadd_Cut ,"" , 10000000000 , 0 );
  return InvMofBplus_h;
}

//\\ ================================================ //\\ ================================================ //\\
//\\ ================================================ //\\ ================================================ //\\
//\\ ================================================ //\\ ================================================ //\\

void BplusYield()
{
  TString InvMass_h_name = "InvMass_h";
  TString InvMass_str = "sqrt( Bs_M*Bs_M + 105.658*105.658 + 2*( Bs_PE*muon_p_NIsoTr_PE - ( Bs_PX*muon_p_NIsoTr_PX  + Bs_PY*muon_p_NIsoTr_PY  +  Bs_PZ*muon_p_NIsoTr_PZ  ) ) )";
  
  // Analyze MC B+ - >JpsiK+ sample : 
  TTree *MC2012_tree      = PrepareTrees( MC_BuJpsiK , true);
  TH1D* bplusMass_MC_h    = GetMyVarHisto( MC2012_tree , InvMass_str , "InvMass_MC_h" , "1" ,  2100 , 5450);
  TCanvas *c_BplusMass_MC = new TCanvas("c_BplusMass_MC","c_BplusMass_MC");
  c_BplusMass_MC.cd();
  bplusMass_MC_h.Draw();
  
  // 
  TH1D* JpsiMass_MC_h     = GetMyVarHisto(MC2012_tree , "muon_p_PAIR_M" , "Jpsi_M_h" , "1", 3000 , 3200);
  TCanvas *c_JpsiMass_MC  = new TCanvas("c_JpsiMass_MC","c_JpsiMass_MC");
  c_JpsiMass_MC.cd();
  JpsiMass_MC_h.Draw();
  
  TChain *EvtTpl_tree = new TChain("EvtTpl_tree","EvtTpl_tree");
  EvtTpl_tree.Add( MC_BuJpsiK_EvtTuple );
  double N_evtAnalyzed = EvtTpl_tree.GetEntries();
  cout<<"N_evtAnalyzed = "<< N_evtAnalyzed <<endl;
  double N_passed = MC2012_tree.GetEntries();
  cout<<"N_passed = "<< N_passed <<endl;
  double eff_strip = N_passed/N_evtAnalyzed;
  cout<<"Eff. of stripping on B+ bkg = "<< eff_strip  << " +/- " << EffErr(N_evtAnalyzed,N_passed) <<endl;
  
  // Analyze data sample :  
  TTree *newOSdata_2012_tree = PrepareTrees( OS_data );
  TH1D* bplusMass_data_h     = GetMyVarHisto(newOSdata_2012_tree , InvMass_str , InvMass_h_name , "1", 2100 , 5450);
  TCanvas *c_BplusMass_data  = new TCanvas("c_BplusMass_data","c_BplusMass_data");
  c_BplusMass_data.cd();
  bplusMass_data_h.Draw();
  //
  TH1D* JpsiMass_data_h     = GetMyVarHisto(newOSdata_2012_tree , "muon_p_PAIR_M" , "Jpsi_M_h" , "1" , 3000 , 3200);
  TCanvas *c_JpsiMass_data  = new TCanvas("c_JpsiMass_data","c_JpsiMass_data");
  c_JpsiMass_data.cd();
  JpsiMass_data_h.Draw();
  
  // Account for MC/data diffs. via a simple weighting :
  TH1D* TrMult_data_h    = GetMyVarHisto(newOSdata_2012_tree , "nTracks" , "nTracks_data", "5250 <"+ InvMass_str+ "&& " + InvMass_str+ "<5350" , 0 , 500);
  TH1D* TrMult_MC_h      = GetMyVarHisto(MC2012_tree         , "nTracks" , "nTracks_MC"  , "5250 <"+ InvMass_str+ "&& " + InvMass_str+ "<5350" , 0 , 500);
  TrMult_data_h.Scale(1./TrMult_data_h.Integral());TrMult_data_h.SetLineColor(kRed);
  TrMult_MC_h.Scale(1./TrMult_MC_h.Integral());TrMult_MC_h.SetLineColor(kBlue);
  
  TH1D *Tr_w_h =  new TH1D( "Tr_w_h" , "Tr_w_h" , 100 , 0 , 500);
  Tr_w_h.Divide(TrMult_data_h,TrMult_MC_h,1,1);
  Tr_w_h.SetLineColor(kMagenta);
  //Tr_w_h.Scale(1./Tr_w_h.Integral());
  
  TCanvas *c_TrMult_comp = new TCanvas("c_TrMult_comp","c_TrMult_comp");
  TrMult_data_h.SetStats(0);
  TrMult_data_h.Draw("ep");
  TrMult_MC_h.Draw("epsame");
  //  Tr_w_h.Draw("epsame");
  c_TrMult_comp.BuildLegend();
  
  //
  TH1D *IsoCriterion_var_histo   = new TH1D("IsoCriterion_var_histo","IsoCriterion_var_histo" ,  100 , -0.5 , 1.5) ;
  TH1D *IsoCriterion_var_w_histo = new TH1D("IsoCriterion_var_w_histo","IsoCriterion_var_w_histo" , 100 , -0.5 , 1.5 ) ;
  TH1D *TMVA_charge_BDT_histo    = new TH1D("TMVA_charge_BDT_histo","TMVA_charge_BDT_histo" ,  100  , -1  ,  +1 ) ;
  TH1D *TMVA_charge_BDT_w_histo  = new TH1D("TMVA_charge_BDT_w_histo","TMVA_charge_BDT_w_histo" , 100 ,-1  , +1 ) ;
  
  Int_t trackmult , muon_p_NIsoTr_TRUEID , muon_p_NIsoTr_MC_MOTHER_ID , 
    muon_p_NIsoTr_MC_GD_MOTHER_ID , muon_p_NIsoTr_MC_MOTHER_KEY , 
    muon_p_MC_MOTHER_KEY , kaon_m_MC_MOTHER_KEY , muon_p_MC_GD_MOTHER_KEY ;
  Float_t TMVA_charge_BDT;
  
  MC2012_tree.SetBranchAddress("nTracks", &trackmult ) ;
  MC2012_tree.SetBranchAddress("muon_p_NIsoTr_TRUEID", &muon_p_NIsoTr_TRUEID ) ;
  MC2012_tree.SetBranchAddress("muon_p_NIsoTr_MC_MOTHER_ID", &muon_p_NIsoTr_MC_MOTHER_ID ) ;
  MC2012_tree.SetBranchAddress("muon_p_NIsoTr_MC_GD_MOTHER_ID", &muon_p_NIsoTr_MC_GD_MOTHER_ID ) ;
  MC2012_tree.SetBranchAddress("muon_p_NIsoTr_MC_MOTHER_KEY", &muon_p_NIsoTr_MC_MOTHER_KEY ) ;
  MC2012_tree.SetBranchAddress("muon_p_MC_MOTHER_KEY", &muon_p_MC_MOTHER_KEY ) ;
  MC2012_tree.SetBranchAddress("kaon_m_MC_MOTHER_KEY", &kaon_m_MC_MOTHER_KEY ) ;
  MC2012_tree.SetBranchAddress("muon_p_MC_GD_MOTHER_KEY", &muon_p_MC_GD_MOTHER_KEY ) ;
  MC2012_tree.SetBranchAddress("TMVA_charge_BDT", &TMVA_charge_BDT ) ;
  
  double weight        = 1;
  int IsoCriterion_var = 0 ;
  // Loop over the cut, trimmed tree, weight in bins of track multiplicity , estimate the integral of the *weighted* histos ... 
  for (int i = 0 ; i < N_passed ; i++)
  {
    MC2012_tree.GetEntry(i);
    
    int weight_bin = Tr_w_h.FindBin(trackmult);
    weight         = Tr_w_h.GetBinContent(weight_bin);
    
    if(abs(muon_p_NIsoTr_TRUEID) == 13 && 
       muon_p_NIsoTr_MC_MOTHER_ID==443 && 
       abs(muon_p_NIsoTr_MC_GD_MOTHER_ID)== 521 &&  
       muon_p_NIsoTr_MC_MOTHER_KEY==muon_p_MC_MOTHER_KEY && 
       kaon_m_MC_MOTHER_KEY==muon_p_MC_GD_MOTHER_KEY ) 
    { IsoCriterion_var = 1.;  }
    else 
    { IsoCriterion_var = 0;  }
    
    IsoCriterion_var_histo   .Fill( IsoCriterion_var ) ;
    IsoCriterion_var_w_histo .Fill( IsoCriterion_var , weight ) ;
    TMVA_charge_BDT_histo    .Fill( TMVA_charge_BDT  ) ;
    TMVA_charge_BDT_w_histo  .Fill( TMVA_charge_BDT  , weight ) ;
  }
  
  // Now estimate the integral of the Old, New BDT, ISO criterion 
  double Eff_Isocriterion_no_w = (double)IsoCriterion_var_histo  .Integral( IsoCriterion_var_histo.GetXaxis().FindBin(0.5) , 
                                                                             IsoCriterion_var_histo.GetXaxis().FindBin(1.5))/
    IsoCriterion_var_histo  .Integral()   ;
  double Eff_Isocriterion_w    = (double)IsoCriterion_var_w_histo.Integral( IsoCriterion_var_w_histo.GetXaxis().FindBin(0.5),  
                                                                             IsoCriterion_var_w_histo.GetXaxis().FindBin(1.5))/
    IsoCriterion_var_w_histo  .Integral()  ;
  double Eff_ChargeBDT_no_w    = (double)TMVA_charge_BDT_histo   .Integral( TMVA_charge_BDT_histo.GetXaxis().FindBin(0.107) , 
                                                                             TMVA_charge_BDT_histo.GetXaxis().FindBin(+1))/\
    TMVA_charge_BDT_histo   .Integral()   ;
  double Eff_ChargeBDT_w       = (double)TMVA_charge_BDT_w_histo .Integral( TMVA_charge_BDT_w_histo.GetXaxis().FindBin(0.107) , 
                                                                             TMVA_charge_BDT_w_histo.GetXaxis().FindBin(+1))/\
    TMVA_charge_BDT_w_histo   .Integral()   ;
  
  cout<<"Eff_Isocriterion_no_w "<< Eff_Isocriterion_no_w << endl;
  cout<<"Eff_Isocriterion_w    "<< Eff_Isocriterion_w    << endl;
  cout<<"Eff_ChargeBDT_no_w    "<< Eff_ChargeBDT_no_w    << endl;
  cout<<"Eff_ChargeBDT_w       "<< Eff_ChargeBDT_w       << endl;

  TCanvas *isociterion_c = new TCanvas("isociterion_c" , "isociterion_c" );
  IsoCriterion_var_histo.SetLineColor(kRed);
  IsoCriterion_var_w_histo.SetLineColor(kBlue);
  IsoCriterion_var_histo.Draw();
  IsoCriterion_var_w_histo.Draw("same");
  isociterion_c.BuildLegend();
  
  TCanvas *TMVA_charge_BDT_c = new TCanvas("TMVA_charge_BDT_c" , "TMVA_charge_BDT_c" );
  TMVA_charge_BDT_histo.SetLineColor(kRed);
  TMVA_charge_BDT_w_histo.SetLineColor(kBlue);
  TMVA_charge_BDT_histo.Draw();
  TMVA_charge_BDT_w_histo.Draw("same");
  TMVA_charge_BDT_c.BuildLegend();
  
  // Save results to .root file
  TFile *Bplus_indata_f = new TFile("../outputs/Bplus_dataNoBDTCut_f.root" , "RECREATE");// 
  Tr_w_h.Write();
  IsoCriterion_var_histo.Write();
  IsoCriterion_var_w_histo.Write();
  TMVA_charge_BDT_histo.Write();
  TMVA_charge_BDT_w_histo.Write();
  bplusMass_MC_h.Write();
  bplusMass_data_h.Write();
  JpsiMass_data_h.Write();
  Bplus_indata_f.Close();
  
  // recycables 
  // Compare distributions
  // CompareVars( newOSdata_2012_tree , newSSdata_2012_tree  );
  // GetSSCrossOS( newOSdata_2012_tree , newSSdata_2012_tree );
  
  return ;
}
